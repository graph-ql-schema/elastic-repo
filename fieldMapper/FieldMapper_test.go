package fieldMapper

import (
	"encoding/json"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/converter"
	"github.com/olivere/elastic/v7"
)

// Тестирование конвертации названия поля
func TestFieldMapper_Map(t *testing.T) {
	type fields struct {
		fieldsMap      map[string]string
		valueConverter converter.ConverterInterface
	}
	type args struct {
		field string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование присутствующего в карте поля",
			fields: fields{
				fieldsMap: map[string]string{
					"id": "_id",
				},
				valueConverter: converterMock{},
			},
			args: args{
				field: "id",
			},
			want: "_id",
		},
		{
			name: "Тестирование отсутствующего в карте поля",
			fields: fields{
				fieldsMap: map[string]string{
					"id": "_id",
				},
				valueConverter: converterMock{},
			},
			args: args{
				field: "name",
			},
			want: "name",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := fieldMapper{
				fieldsMap: tt.fields.fieldsMap,
			}
			if got := f.Map(tt.args.field); got != tt.want {
				t.Errorf("Map() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга результатов поиска
func Test_fieldMapper_ParseSearchRequest(t *testing.T) {
	data, _ := json.Marshal(map[string]interface{}{
		"name": "test",
	})

	type fields struct {
		fieldsMap      map[string]string
		valueConverter converter.ConverterInterface
	}
	type args struct {
		result *elastic.SearchResult
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []map[string]interface{}
	}{
		{
			name: "Тестирование результатов поиска",
			fields: fields{
				fieldsMap: map[string]string{
					"id": "_id",
				},
				valueConverter: converterMock{},
			},
			args: args{
				result: &elastic.SearchResult{
					Hits: &elastic.SearchHits{
						Hits: []*elastic.SearchHit{
							{
								Index:  "test",
								Type:   "_doc",
								Id:     "1",
								Uid:    "u1",
								Score:  nil,
								Source: data,
								Shard:  "1",
								Node:   "n1",
							},
							{
								Index:  "test",
								Type:   "_doc",
								Id:     "2",
								Uid:    "u2",
								Score:  nil,
								Source: data,
								Shard:  "1",
								Node:   "n1",
							},
						},
					},
				},
			},
			want: []map[string]interface{}{
				{
					"id":   "1",
					"name": "test",
				},
				{
					"id":   "2",
					"name": "test",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := fieldMapper{
				fieldsMap:      tt.fields.fieldsMap,
				valueConverter: tt.fields.valueConverter,
			}
			if got := f.ParseSearchRequest(tt.args.result); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseSearchRequest() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование маппинга полей сущности
func Test_fieldMapper_MapEntityToInsertOrUpdate(t *testing.T) {
	type fields struct {
		fieldsMap      map[string]string
		valueConverter converter.ConverterInterface
	}
	type args struct {
		entity map[string]interface{}
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantMappedEntity map[string]interface{}
		wantId           string
		wantErr          bool
	}{
		{
			name: "Тестирование маппинга поля без кастомизации",
			fields: fields{
				fieldsMap: map[string]string{
					"id":  "_id",
					"idx": "_index",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"test": "test1",
				},
			},
			wantMappedEntity: map[string]interface{}{
				"test": "test1",
			},
			wantId:  "",
			wantErr: false,
		},
		{
			name: "Тестирование маппинга поля с кастомизацией",
			fields: fields{
				fieldsMap: map[string]string{
					"id":   "_id",
					"idx":  "_index",
					"name": "test_name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"name": "test1",
				},
			},
			wantMappedEntity: map[string]interface{}{
				"test_name": "test1",
			},
			wantId:  "",
			wantErr: false,
		},
		{
			name: "Тестирование маппинга пустого ID",
			fields: fields{
				fieldsMap: map[string]string{
					"id":   "_id",
					"idx":  "_index",
					"name": "test_name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"_id": "",
				},
			},
			wantMappedEntity: nil,
			wantId:           "",
			wantErr:          true,
		},
		{
			name: "Тестирование маппинга корректного ID",
			fields: fields{
				fieldsMap: map[string]string{
					"id":   "_id",
					"idx":  "_index",
					"name": "test_name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"_id": "Test",
				},
			},
			wantMappedEntity: map[string]interface{}{},
			wantId:           "Test",
			wantErr:          false,
		},
		{
			name: "Тестирование маппинга корректного ID с маппингом поля",
			fields: fields{
				fieldsMap: map[string]string{
					"id":   "_id",
					"idx":  "_index",
					"name": "test_name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"id": "Test",
				},
			},
			wantMappedEntity: map[string]interface{}{},
			wantId:           "Test",
			wantErr:          false,
		},
		{
			name: "Тестирование маппинга системного поля",
			fields: fields{
				fieldsMap: map[string]string{
					"id":   "_id",
					"idx":  "_index",
					"name": "test_name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"_index": "Test",
				},
			},
			wantMappedEntity: map[string]interface{}{},
			wantId:           "",
			wantErr:          false,
		},
		{
			name: "Тестирование маппинга системного поля с маппингом поля",
			fields: fields{
				fieldsMap: map[string]string{
					"id":   "_id",
					"idx":  "_index",
					"name": "test_name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				entity: map[string]interface{}{
					"idx": "Test",
				},
			},
			wantMappedEntity: map[string]interface{}{},
			wantId:           "",
			wantErr:          false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := fieldMapper{
				fieldsMap:      tt.fields.fieldsMap,
				valueConverter: tt.fields.valueConverter,
			}
			gotMappedEntity, gotId, err := f.MapEntityToInsertOrUpdate(tt.args.entity)
			if (err != nil) != tt.wantErr {
				t.Errorf("MapEntityToInsertOrUpdate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotMappedEntity, tt.wantMappedEntity) {
				t.Errorf("MapEntityToInsertOrUpdate() gotMappedEntity = %v, want %v", gotMappedEntity, tt.wantMappedEntity)
			}
			if gotId != tt.wantId {
				t.Errorf("MapEntityToInsertOrUpdate() gotId = %v, want %v", gotId, tt.wantId)
			}
		})
	}
}

// Тестирование парсинга ответа для Get запроса
func Test_fieldMapper_ParseGetRequest(t *testing.T) {
	data, _ := json.Marshal(map[string]interface{}{
		"name": "test",
	})

	type fields struct {
		fieldsMap      map[string]string
		valueConverter converter.ConverterInterface
	}
	type args struct {
		result *elastic.GetResult
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   map[string]interface{}
	}{
		{
			name: "Тестирование на пустом значении",
			fields: fields{
				fieldsMap: map[string]string{
					"id": "_id",
				},
				valueConverter: converterMock{},
			},
			args: args{
				result: nil,
			},
			want: nil,
		},
		{
			name: "Тестирование на валидном результате",
			fields: fields{
				fieldsMap: map[string]string{
					"id":           "_id",
					"entity_title": "name",
				},
				valueConverter: converterMock{},
			},
			args: args{
				result: &elastic.GetResult{
					Index:  "test",
					Type:   "_doc",
					Id:     "1",
					Uid:    "u1",
					Source: data,
				},
			},
			want: map[string]interface{}{
				"id":           "1",
				"entity_title": "test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := fieldMapper{
				fieldsMap:      tt.fields.fieldsMap,
				valueConverter: tt.fields.valueConverter,
			}
			if got := f.ParseGetRequest(tt.args.result); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseGetRequest() = %v, want %v", got, tt.want)
			}
		})
	}
}
