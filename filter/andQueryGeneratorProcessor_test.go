package filter

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование доступности процессора
func Test_andQueryGeneratorProcessor_isAvailable(t *testing.T) {
	type fields struct {
		multiProcessor multiProcessorInterface
	}
	type args struct {
		params whereOrHavingParser.Operation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не доступной операции",
			fields: fields{
				multiProcessor: &multiProcessorMock{
					Result:  nil,
					IsError: true,
				},
			},
			args: args{
				params: operationMock{
					rType: "test",
				},
			},
			want: false,
		},
		{
			name: "Тестирование на доступной операции",
			fields: fields{
				multiProcessor: &multiProcessorMock{
					Result:  nil,
					IsError: true,
				},
			},
			args: args{
				params: operationMock{
					rType: constants.AndSchemaKey,
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := andQueryGeneratorProcessor{
				multiProcessor: tt.fields.multiProcessor,
			}
			if got := a.isAvailable(tt.args.params); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_andQueryGeneratorProcessor_generate(t *testing.T) {
	type fields struct {
		multiProcessor multiProcessorInterface
	}
	type args struct {
		params whereOrHavingParser.Operation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    elastic.Query
		wantErr bool
	}{
		{
			name: "Тестирование генерации с возвращением ошибки мультипроцессора",
			fields: fields{
				multiProcessor: &multiProcessorMock{
					Result:  nil,
					IsError: true,
				},
			},
			args: args{
				params: operationMock{
					rType: constants.AndSchemaKey,
					rValue: []whereOrHavingParser.Operation{
						operationMock{},
						operationMock{},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации без ошибки мультипроцессора",
			fields: fields{
				multiProcessor: &multiProcessorMock{
					Result: []elastic.Query{
						elastic.NewTermQuery("test", "val"),
						elastic.NewTermQuery("test", "val"),
					},
					IsError: false,
				},
			},
			args: args{
				params: operationMock{
					rType: constants.AndSchemaKey,
					rValue: []whereOrHavingParser.Operation{
						operationMock{},
						operationMock{},
					},
				},
			},
			want: elastic.NewBoolQuery().Must(
				elastic.NewTermQuery("test", "val"),
				elastic.NewTermQuery("test", "val"),
			),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := andQueryGeneratorProcessor{
				multiProcessor: tt.fields.multiProcessor,
			}
			got, err := a.generate(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
