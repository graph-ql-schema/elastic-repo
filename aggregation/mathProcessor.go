package aggregation

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

var mathFieldCodes = []string{
	constants.AvgOperationSchemaKey,
	constants.SumOperationSchemaKey,
	constants.MinOperationSchemaKey,
	constants.MaxOperationSchemaKey,
}

// Процессор генерации математических агрегаций
type mathProcessor struct {
	fieldMapper fieldMapper.FieldMapperInterface
}

// Проверка доступности процессора генерации
func (m mathProcessor) isGenerationAvailable(params sbuilder.Parameters) bool {
	for _, fieldCode := range mathFieldCodes {
		for _, field := range params.Fields {
			if fieldCode == field.GetFieldName() {
				return true
			}
		}
	}

	return false
}

// Генерация запроса
func (m mathProcessor) generateQuery(params sbuilder.Parameters) []GenerationResult {
	result := []GenerationResult{}
	for _, fieldCode := range mathFieldCodes {
		for _, field := range params.Fields {
			if fieldCode != field.GetFieldName() || !field.HasSubFields() {
				continue
			}

			for _, subField := range field.GetSubFields() {
				mappedField := m.fieldMapper.Map(subField.GetFieldName())

				var aggregation elastic.Aggregation
				switch fieldCode {
				case constants.AvgOperationSchemaKey:
					aggregation = elastic.NewAvgAggregation().Field(mappedField)
					break
				case constants.SumOperationSchemaKey:
					aggregation = elastic.NewSumAggregation().Field(mappedField)
					break
				case constants.MinOperationSchemaKey:
					aggregation = elastic.NewMinAggregation().Field(mappedField)
					break
				case constants.MaxOperationSchemaKey:
					aggregation = elastic.NewMaxAggregation().Field(mappedField)
					break
				}

				if nil == aggregation {
					continue
				}

				result = append(result, GenerationResult{
					Name:        m.getAggregationCode(mappedField, fieldCode),
					Aggregation: aggregation,
				})
			}
		}
	}

	return result
}

// Парсинг результата
func (m mathProcessor) parseResult(
	params sbuilder.Parameters,
	result *AggregationResponse,
	data *elastic.AggregationBucketKeyItem,
) {
	for _, fieldCode := range mathFieldCodes {
		for _, field := range params.Fields {
			if fieldCode != field.GetFieldName() || !field.HasSubFields() {
				continue
			}

			for _, subField := range field.GetSubFields() {
				mappedField := m.fieldMapper.Map(subField.GetFieldName())

				switch fieldCode {
				case constants.AvgOperationSchemaKey:
					if nil == result.Avg {
						result.Avg = map[string]float64{}
					}

					if res, ok := data.Avg(m.getAggregationCode(mappedField, fieldCode)); ok {
						result.Avg[subField.GetFieldName()] = *res.Value
					}
					break
				case constants.SumOperationSchemaKey:
					if nil == result.Sum {
						result.Sum = map[string]float64{}
					}

					if res, ok := data.Sum(m.getAggregationCode(mappedField, fieldCode)); ok {
						result.Sum[subField.GetFieldName()] = *res.Value
					}
					break
				case constants.MinOperationSchemaKey:
					if nil == result.Min {
						result.Min = map[string]float64{}
					}

					if res, ok := data.Min(m.getAggregationCode(mappedField, fieldCode)); ok {
						result.Min[subField.GetFieldName()] = *res.Value
					}
					break
				case constants.MaxOperationSchemaKey:
					if nil == result.Max {
						result.Max = map[string]float64{}
					}

					if res, ok := data.Max(m.getAggregationCode(mappedField, fieldCode)); ok {
						result.Max[subField.GetFieldName()] = *res.Value
					}
					break
				}
			}
		}
	}
}

// Генерация уникального кода агрегации по переданным параметрам названия поля и операции
func (m mathProcessor) getAggregationCode(fieldName string, aggregation string) string {
	return fmt.Sprintf(`%v_aggregation_operation_%v`, fieldName, aggregation)
}

// Фабрика процессора
func newMathProcessor(object *graphql.Object, fieldsMap map[string]string) aggregationProcessorInterface {
	return &mathProcessor{
		fieldMapper: fieldMapper.NewFieldMapper(object, fieldsMap),
	}
}
