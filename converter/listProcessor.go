package converter

import (
	"fmt"
	"reflect"
	"strings"
	"sync"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

// Процессор конвертации массивов значений
type listProcessor struct {
	object     *graphql.Object
	converter  gql_sql_converter.GraphQlSqlConverterInterface
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Тестирование доступности процессора
func (l listProcessor) isAvailable(field string) bool {
	fieldObj, ok := l.object.Fields()[field]
	if !ok {
		return false
	}

	return l.typeGetter.IsList(fieldObj.Type)
}

// Конвертация значения
func (l listProcessor) convert(field string, val interface{}) (interface{}, error) {
	if reflect.TypeOf(val).Kind() != reflect.Slice {
		val = []interface{}{val}
	}

	var wg sync.WaitGroup
	itemChan := make(chan interface{})
	errChan := make(chan error)
	resultChan := make(chan []interface{})
	errResultChan := make(chan error)
	completeChan := make(chan bool)

	defer close(itemChan)
	defer close(errChan)
	defer close(resultChan)
	defer close(errResultChan)
	defer close(completeChan)

	// Запускаем чтение результатов
	go l.readValues(itemChan, errChan, resultChan, errResultChan, completeChan)

	s := reflect.ValueOf(val)

	// Запускаем асинхронную обработку значений
	wg.Add(s.Len())
	for i := 0; i < s.Len(); i++ {
		go l.convertSingleValue(&wg, itemChan, errChan, field, s.Index(i).Interface())
	}

	wg.Wait()
	completeChan <- true

	items := <-resultChan
	err := <-errResultChan

	return items, err
}

// Чтение обработанных значений
func (l listProcessor) readValues(
	itemChan chan interface{},
	errChan chan error,
	resultChan chan []interface{},
	errResultChan chan error,
	completeChan chan bool,
) {
	items := []interface{}{}
	errors := []error{}

	for {
		select {
		case item := <-itemChan:
			items = append(items, item)
			break
		case err := <-errChan:
			if nil != err {
				errors = append(errors, err)
			}
			break
		case _ = <-completeChan:
			messages := []string{}
			for i, err := range errors {
				messages = append(messages, fmt.Sprintf(`%v) %v;`, i+1, err.Error()))
			}

			var err error
			if 0 != len(messages) {
				items = []interface{}{}
				err = fmt.Errorf(`some errors occured: %v`, strings.Join(messages, " "))
			}

			resultChan <- items
			errResultChan <- err

			return
		}
	}
}

// Асинхронная конвертация единичного значения
func (l listProcessor) convertSingleValue(
	wg *sync.WaitGroup,
	itemChan chan interface{},
	errChan chan error,
	field string,
	val interface{},
) {
	defer wg.Done()

	item, err := l.converter.ToBaseType(l.object, field, val)

	itemChan <- item
	errChan <- err
}

// Фабрика процессора
func newListProcessor(object *graphql.Object) converterProcessorInterface {
	return &listProcessor{
		object:     object,
		converter:  gql_sql_converter.NewGraphQlSqlConverter(),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
