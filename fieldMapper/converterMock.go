package fieldMapper

// Подставка для тестирования
type converterMock struct{}

// Конвертация значений
func (c converterMock) Convert(field string, val interface{}) (interface{}, error) {
	return val, nil
}
