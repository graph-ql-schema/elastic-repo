package converter

import (
	"github.com/graphql-go/graphql"
)

// Заглушка для тестирования
type graphQlSqlConverterStub struct{}

// Конвертация в базовый тип, например в строку или число
func (g graphQlSqlConverterStub) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	return value, nil
}

// Конвертация в SQL like значение
func (g graphQlSqlConverterStub) ToSQLValue(*graphql.Object, string, interface{}) (string, error) {
	return "", nil
}
