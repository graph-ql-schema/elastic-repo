package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type multiProcessorMock struct {
	Result  []elastic.Query
	IsError bool
}

// Установка генератора для парсинга
func (m multiProcessorMock) setGenerator(QueryGeneratorInterface) {}

// Обработка переданных сущностей
func (m multiProcessorMock) process([]whereOrHavingParser.Operation) ([]elastic.Query, error) {
	if m.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return m.Result, nil
}
