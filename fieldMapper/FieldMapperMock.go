package fieldMapper

import (
	"fmt"

	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type FieldMapperMock struct {
	SearchResult             []map[string]interface{}
	GetResult                map[string]interface{}
	IsMapEntityToInsertError bool
	MapEntityToInsertId      string
}

func (f FieldMapperMock) ParseGetRequest(*elastic.GetResult) map[string]interface{} {
	return f.GetResult
}

func (f FieldMapperMock) MapEntityToInsertOrUpdate(e map[string]interface{}) (mappedEntity map[string]interface{}, id string, err error) {
	if f.IsMapEntityToInsertError {
		return nil, "", fmt.Errorf(`test`)
	}

	return e, f.MapEntityToInsertId, nil
}

// Парсинг результатов поиска. Подменяет поля на имеющиеся в маппинге и возвращает результат
func (f FieldMapperMock) ParseSearchRequest(*elastic.SearchResult) []map[string]interface{} {
	return f.SearchResult
}

// Получение корректного названия поля
func (f FieldMapperMock) Map(field string) string {
	return field
}
