package executor

import (
	"fmt"

	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type QueryExecutorMock struct {
	IsSearchError     bool
	SearchResult      *elastic.SearchResult
	IsGetByIdError    bool
	GetByIdResult     *elastic.GetResult
	IsDeleteByIdError bool
	IsInsertError     bool
	InsertId          string
	IsUpdateError     bool
}

// Выполнение запроса обновления сущности в ElasticSearch
func (q QueryExecutorMock) ExecuteUpdateQuery(string, string, interface{}, []configurable_logger.LogOperation) error {
	if q.IsUpdateError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Выполнение запроса вставки сущности в ElasticSearch
func (q QueryExecutorMock) ExecuteInsertQuery(string, string, interface{}, []configurable_logger.LogOperation) (insertedId string, err error) {
	if q.IsInsertError {
		return "", fmt.Errorf(`test`)
	}

	return q.InsertId, nil
}

// Выполнение запроса удаления сущности по ID в ElasticSearch
func (q QueryExecutorMock) ExecuteRemoveByIdQuery(string, string, []configurable_logger.LogOperation) error {
	if q.IsDeleteByIdError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Выполнение запроса получения сущности по ID в ElasticSearch
func (q QueryExecutorMock) ExecuteGetByIdQuery(string, string, []configurable_logger.LogOperation) (*elastic.GetResult, error) {
	if q.IsGetByIdError {
		return nil, fmt.Errorf(`test`)
	}

	return q.GetByIdResult, nil
}

// Выполнение запроса поиска в ElasticSearch
func (q QueryExecutorMock) ExecuteSearchQuery(string, *elastic.SearchSource, []configurable_logger.LogOperation) (*elastic.SearchResult, error) {
	if q.IsSearchError {
		return nil, fmt.Errorf(`test`)
	}

	return q.SearchResult, nil
}
