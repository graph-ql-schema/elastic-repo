package client

import (
	"context"
	"fmt"
	"os"
	"reflect"
	"sort"
	"strings"
	"sync"
	"time"

	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Статус проверки сервера
type serverCheckStatus struct {
	IsAvailable bool
	Server      string
}

// Клиент ElasticSearch
type elasticClientService struct {
	logFactory         configurable_logger.TLoggerFactory
	logger             configurable_logger.LoggerInterface
	logIncreaseService configurable_logger.LoggerOperationIncreaseServiceInterface

	client  *elastic.Client
	context context.Context

	servers  []string
	login    string
	password string

	checkStarted bool
	lastStatuses []serverCheckStatus
}

// Получение актуального клиента
func (e elasticClientService) GetClient() *elastic.Client {
	if nil == e.client {
		e.refreshClient(nil, e.lastStatuses)
	}

	return e.client
}

// Получение контекста запросов
func (e elasticClientService) GetContext() context.Context {
	return e.context
}

// Остановка сервиса
func (e *elasticClientService) GracefulShutdown(operations []configurable_logger.LogOperation) {
	if false == e.checkStarted {
		return
	}

	ops := e.logIncreaseService.IncreaseOperations(operations)
	e.logger.Debug(configurable_logger.LogParameters{
		Code:       200,
		Message:    "Stopping client automatics refreshing",
		Operations: ops,
	})

	e.checkStarted = false
}

// Запуск сервиса. Позволяет обновлять клиент всякий раз, когда один из серверов не доступен
func (e *elasticClientService) RunServerCheck(operations []configurable_logger.LogOperation) error {
	ops := e.logIncreaseService.IncreaseOperations(operations)

	e.checkStarted = true
	e.lastStatuses = nil

	e.logger.Info(configurable_logger.LogParameters{
		Code:    200,
		Message: "Starting client automatics refreshing",
	})

	e.refreshClient(ops, e.lastStatuses)

	return e.checkServers(ops)
}

// Проверка серверов на доступность
func (e *elasticClientService) checkServers(operations []configurable_logger.LogOperation) error {
	var wg sync.WaitGroup
	ops := e.logIncreaseService.IncreaseOperations(operations)

	statusesResult := make(chan []serverCheckStatus)
	statuses := make(chan serverCheckStatus)
	complete := make(chan bool)

	defer close(statuses)
	defer close(complete)
	defer close(statusesResult)

	for {
		if false == e.checkStarted {
			return nil
		}

		// Запуск чтения результатов проверки
		go e.readCheckResults(statusesResult, statuses, complete)

		// Запускаем проверку серверов
		wg.Add(len(e.servers))
		for _, server := range e.servers {
			go e.checkServerAsync(server, ops, &wg, statuses)
		}

		wg.Wait()
		complete <- true

		bufferedStatuses := <-statusesResult
		if false == reflect.DeepEqual(e.lastStatuses, bufferedStatuses) {
			available := 0
			for _, status := range bufferedStatuses {
				if status.IsAvailable {
					available = available + 1
				}
			}

			e.logger.Notice(configurable_logger.LogParameters{
				Code:    300,
				Message: fmt.Sprintf("Refreshing client. Available %v servers", available),
			})

			e.refreshClient(ops, bufferedStatuses)
		}

		resultStatus := false
		for _, status := range bufferedStatuses {
			resultStatus = resultStatus || status.IsAvailable
		}

		if false == resultStatus {
			err := fmt.Errorf(`noone server is available`)
			e.logger.Error(configurable_logger.LogParameters{
				Code:       500,
				Message:    fmt.Sprintf("Servers failes to check: %v", err.Error()),
				Operations: ops,
			})

			return err
		}

		e.lastStatuses = bufferedStatuses
		time.Sleep(1 * time.Second)
	}
}

// Обновление клиента
func (e *elasticClientService) refreshClient(operations []configurable_logger.LogOperation, statuses []serverCheckStatus) {
	ops := e.logIncreaseService.IncreaseOperations(operations)
	servers := []string{}
	if 0 != len(statuses) {
		for _, status := range statuses {
			if status.IsAvailable {
				servers = append(servers, status.Server)
			}
		}
	} else {
		servers = e.servers
	}

	e.logger.Debug(configurable_logger.LogParameters{
		Code:       301,
		Message:    fmt.Sprintf("Refreshing ElasticSearch client. Servers: (%v)", strings.Join(servers, ", ")),
		Operations: ops,
	})

	client, err := elastic.NewClient(
		elastic.SetURL(servers...),
		elastic.SetBasicAuth(e.login, e.password),
	)

	if nil != err {
		e.logger.Error(configurable_logger.LogParameters{
			Code:    505,
			Message: "Failed to create ElasticSearch client",
		})

		os.Exit(2)
	}

	e.client = client
}

// Запуск чтения статусов проверки серверов
func (e elasticClientService) readCheckResults(
	statusesResult chan []serverCheckStatus,
	statuses chan serverCheckStatus,
	complete chan bool,
) {
	result := []serverCheckStatus{}
	for {
		select {
		case _ = <-complete:
			sort.SliceStable(result, func(i, j int) bool {
				return result[i].Server > result[j].Server
			})

			statusesResult <- result
			return
		case status := <-statuses:
			result = append(result, status)
		}
	}
}

// Проверка одного конкретного сервера. Если он не доступен, возвращается ошибка.
func (e elasticClientService) checkServerAsync(
	server string,
	operations []configurable_logger.LogOperation,
	wg *sync.WaitGroup,
	statuses chan serverCheckStatus,
) {
	defer wg.Done()

	err := e.checkServer(server, operations)

	statuses <- serverCheckStatus{
		IsAvailable: nil == err,
		Server:      server,
	}
}

// Проверка одного конкретного сервера. Если он не доступен, возвращается ошибка.
func (e elasticClientService) checkServer(server string, operations []configurable_logger.LogOperation) error {
	ops := e.logIncreaseService.IncreaseOperations(operations)
	logService := e.logFactory(fmt.Sprintf(`elasticClientService(server: %v)`, server))

	info, code, err := e.client.Ping(server).Do(e.context)
	if err != nil {
		logService.Debug(configurable_logger.LogParameters{
			Code:       500,
			Message:    fmt.Sprintf(`Failed to ping server. Error: %v`, err.Error()),
			Operations: ops,
		})

		return err
	}

	logService.Debug(configurable_logger.LogParameters{
		Code:       uint64(code),
		Message:    fmt.Sprintf(`pinged server: status - %v, version - %v`, code, info.Version.Number),
		Operations: ops,
	})

	return nil
}

// Фабрика сервиса клиента elastic
func NewElasticClientService(
	servers []string,
	username string,
	password string,
	ctx context.Context,
	factory configurable_logger.TLoggerFactory,
) ElasticClientServiceInterface {
	return &elasticClientService{
		logFactory:         factory,
		logger:             factory(fmt.Sprintf(`elasticClientService(%v servers)`, len(servers))),
		logIncreaseService: configurable_logger.LoggerOperationIncreaseService(),
		context:            ctx,
		servers:            servers,
		login:              username,
		password:           password,
		checkStarted:       false,
		lastStatuses:       nil,
	}
}
