package queries

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type AggregationQueryBuilderMock struct {
	IsError      bool
	IsParseError bool
	Query        *elastic.SearchSource
	Result       []*aggregation.AggregationResponse
}

// Построение запроса агрегации
func (a AggregationQueryBuilderMock) Build(sbuilder.Parameters, []configurable_logger.LogOperation) (*elastic.SearchSource, error) {
	if a.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return a.Query, nil
}

// Парсинг ответа на запрос
func (a AggregationQueryBuilderMock) Parse(sbuilder.Parameters, *elastic.SearchResult, []configurable_logger.LogOperation) ([]*aggregation.AggregationResponse, error) {
	if a.IsParseError {
		return nil, fmt.Errorf(`test`)
	}

	return a.Result, nil
}
