package aggregation

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/olivere/elastic/v7"
)

// Тестирование доступности
func Test_countProcessor_isGenerationAvailable(t *testing.T) {
	type args struct {
		in0 sbuilder.Parameters
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Процессор должен быть не доступен всегда",
			args: args{},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := countProcessor{}
			if got := c.isGenerationAvailable(tt.args.in0); got != tt.want {
				t.Errorf("isGenerationAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса
func Test_countProcessor_generateQuery(t *testing.T) {
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name string
		args args
		want []GenerationResult
	}{
		{
			name: "Всегда должен возвращаться NULL",
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := countProcessor{}
			if got := c.generateQuery(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generateQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга результата
func Test_countProcessor_parseResult(t *testing.T) {
	type args struct {
		params sbuilder.Parameters
		result *AggregationResponse
		data   *elastic.AggregationBucketKeyItem
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			name: "Тестирование парсинга запроса",
			args: args{
				params: sbuilder.Parameters{},
				result: &AggregationResponse{
					Count:    0,
					Avg:      nil,
					Sum:      nil,
					Min:      nil,
					Max:      nil,
					Variants: nil,
				},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: nil,
					Key:          nil,
					KeyAsString:  nil,
					KeyNumber:    "",
					DocCount:     10,
				},
			},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := countProcessor{}

			if c.parseResult(tt.args.params, tt.args.result, tt.args.data); !reflect.DeepEqual(tt.args.result.Count, tt.want) {
				t.Errorf("parseResult() count = %v, want %v", tt.args.result.Count, tt.want)
			}
		})
	}
}
