package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Генератор параметров фильтрации
type QueryGeneratorMock struct {
	Result  elastic.Query
	IsError bool
}

// Генерация эквивалента переданного запроса фильтрации для ElasticSearch
func (q QueryGeneratorMock) Generate(whereOrHavingParser.Operation) (elastic.Query, error) {
	if q.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return q.Result, nil
}
