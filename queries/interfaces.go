package queries

import (
	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	configurable_logger "bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Генератор запроса агрегации
type AggregationQueryBuilderInterfaces interface {
	// Построение запроса агрегации
	Build(params sbuilder.Parameters, operations []configurable_logger.LogOperation) (*elastic.SearchSource, error)

	// Парсинг ответа на запрос
	Parse(
		params sbuilder.Parameters,
		response *elastic.SearchResult,
		operations []configurable_logger.LogOperation,
	) ([]*aggregation.AggregationResponse, error)
}

// Генератор запросов листинга сущностей
type ListQueryBuilderInterface interface {
	// Построение запроса листинга сущностей
	Build(params sbuilder.Parameters, operations []configurable_logger.LogOperation) (*elastic.SearchSource, error)

	// Парсинг ответа на запрос
	Parse(response *elastic.SearchResult, operations []configurable_logger.LogOperation) []map[string]interface{}
}

// Генератор запросов на получение ID сущностей по переданным параметрам фильтрации
type EntityIdsQueryBuilderInterface interface {
	// Построение запроса листинга сущностей
	Build(filter whereOrHavingParser.Operation, operations []configurable_logger.LogOperation) (*elastic.SearchSource, error)

	// Парсинг ответа на запрос
	Parse(response *elastic.SearchResult, operations []configurable_logger.LogOperation) []string
}
