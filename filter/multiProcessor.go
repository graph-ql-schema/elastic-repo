package filter

import (
	"fmt"
	"strings"
	"sync"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Обработчик множественных сущностей
type multiProcessor struct {
	generator QueryGeneratorInterface
}

// Установка генератора для парсинга
func (m *multiProcessor) setGenerator(generator QueryGeneratorInterface) {
	m.generator = generator
}

// Обработка переданных сущностей
func (m *multiProcessor) process(params []whereOrHavingParser.Operation) ([]elastic.Query, error) {
	if 0 == len(params) {
		return []elastic.Query{}, nil
	}

	var wg sync.WaitGroup
	resultChan := make(chan []elastic.Query)
	errResultChan := make(chan error)
	itemChan := make(chan elastic.Query)
	errChan := make(chan error)
	completeChan := make(chan bool)

	defer close(resultChan)
	defer close(errResultChan)
	defer close(itemChan)
	defer close(errChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов обработки
	go m.readResults(resultChan, errResultChan, itemChan, errChan, completeChan)

	wg.Add(len(params))
	for _, parameter := range params {
		go m.processParameter(&wg, itemChan, errChan, parameter)
	}

	wg.Wait()
	completeChan <- true

	result := <-resultChan
	err := <-errResultChan

	if nil != err {
		return nil, err
	}

	return result, nil
}

// Обработка отдельной операции
func (m *multiProcessor) processParameter(
	wg *sync.WaitGroup,
	itemChan chan elastic.Query,
	errChan chan error,
	parameter whereOrHavingParser.Operation,
) {
	defer wg.Done()

	item, err := m.generator.Generate(parameter)

	itemChan <- item
	errChan <- err
}

// Чтение результатов обработки каждого отдельного подзапроса
func (m *multiProcessor) readResults(
	resultChan chan []elastic.Query,
	errResultChan chan error,
	itemChan chan elastic.Query,
	errChan chan error,
	completeChan chan bool,
) {
	resultErr := []error{}
	results := []elastic.Query{}

	for {
		select {
		case item := <-itemChan:
			if nil != item {
				results = append(results, item)
			}
			break
		case err := <-errChan:
			if nil != err {
				resultErr = append(resultErr, err)
			}
			break
		case _ = <-completeChan:
			if 0 == len(resultErr) {
				resultChan <- results
				errResultChan <- nil

				return
			}

			errors := []string{}
			for i, err := range resultErr {
				errors = append(errors, fmt.Sprintf(`%v) %v;`, i+1, err.Error()))
			}

			resultChan <- []elastic.Query{}
			errResultChan <- fmt.Errorf(`some errors occured: %v`, strings.Join(errors, " "))

			return
		}
	}
}

// Фабрика мультипроцессора
func newMultiProcessor() multiProcessorInterface {
	return &multiProcessor{}
}
