package aggregation

import (
	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type aggregationProcessorMock struct {
	IsAvailable           bool
	QueryResults          []GenerationResult
	ResultProcessCallback func(result *AggregationResponse)
}

// Проверка доступности процессора
func (a aggregationProcessorMock) isGenerationAvailable(sbuilder.Parameters) bool {
	return a.IsAvailable
}

// Генерация запроса
func (a aggregationProcessorMock) generateQuery(sbuilder.Parameters) []GenerationResult {
	return a.QueryResults
}

// Парсинг результата
func (a aggregationProcessorMock) parseResult(params sbuilder.Parameters, result *AggregationResponse, data *elastic.AggregationBucketKeyItem) {
	a.ResultProcessCallback(result)
}
