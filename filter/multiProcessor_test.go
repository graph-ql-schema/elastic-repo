package filter

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование установки генератора
func Test_multiProcessor_setGenerator(t *testing.T) {
	type fields struct {
		generator QueryGeneratorInterface
	}
	type args struct {
		generator QueryGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Тестирование установки генератора",
			fields: fields{},
			args: args{
				generator: QueryGeneratorMock{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &multiProcessor{
				generator: tt.fields.generator,
			}

			m.setGenerator(tt.args.generator)

			if nil == m.generator {
				t.Error(`Generator is not set`)
			}
		})
	}
}

// Тестирование обработки операций
func Test_multiProcessor_process(t *testing.T) {
	type fields struct {
		generator QueryGeneratorInterface
	}
	type args struct {
		params []whereOrHavingParser.Operation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []elastic.Query
		wantErr bool
	}{
		{
			name: "Тестирование передачи пустого набора операций",
			fields: fields{
				generator: QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "val"),
					IsError: false,
				},
			},
			args: args{
				params: []whereOrHavingParser.Operation{},
			},
			want:    []elastic.Query{},
			wantErr: false,
		},
		{
			name: "Тестирование передачи 2 операций, генератор возвращает ошибку",
			fields: fields{
				generator: QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "val"),
					IsError: true,
				},
			},
			args: args{
				params: []whereOrHavingParser.Operation{
					&operationMock{},
					&operationMock{},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование передачи 2 операций, генератор не возвращает ошибку",
			fields: fields{
				generator: QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "val"),
					IsError: false,
				},
			},
			args: args{
				params: []whereOrHavingParser.Operation{
					&operationMock{},
					&operationMock{},
				},
			},
			want: []elastic.Query{
				elastic.NewTermQuery("test", "val"),
				elastic.NewTermQuery("test", "val"),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &multiProcessor{
				generator: tt.fields.generator,
			}
			got, err := m.process(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("process() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("process() got = %v, want %v", got, tt.want)
			}
		})
	}
}
