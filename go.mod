module bitbucket.org/graph-ql-schema/elastic-repo

go 1.14

require (
	bitbucket.org/graph-ql-schema/gql-root-type-getter v1.1.0
	bitbucket.org/graph-ql-schema/gql-sql-converter v1.0.6
	bitbucket.org/graph-ql-schema/sbuilder v1.0.12
	bitbucket.org/sveshnikovwork/configurable-logger v1.1.5
	github.com/graphql-go/graphql v0.7.9
	github.com/olivere/elastic/v7 v7.0.14
	golang.org/x/sys v0.0.0-20200428200454-593003d681fa // indirect
)
