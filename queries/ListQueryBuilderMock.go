package queries

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type ListQueryBuilderMock struct {
	IsBuildError bool
	BuildResult  *elastic.SearchSource
	ParseResult  []map[string]interface{}
}

// Построение запроса листинга сущностей
func (l ListQueryBuilderMock) Build(
	sbuilder.Parameters,
	[]configurable_logger.LogOperation,
) (*elastic.SearchSource, error) {
	if l.IsBuildError {
		return nil, fmt.Errorf(`test`)
	}

	return l.BuildResult, nil
}

// Парсинг ответа на запрос
func (l ListQueryBuilderMock) Parse(
	*elastic.SearchResult,
	[]configurable_logger.LogOperation,
) []map[string]interface{} {
	return l.ParseResult
}
