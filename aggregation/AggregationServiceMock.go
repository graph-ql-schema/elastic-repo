package aggregation

import (
	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type AggregationServiceMock struct {
	QueryResult []GenerationResult
	Parse       *AggregationResponse
}

// Генерация запроса
func (a AggregationServiceMock) GenerateQueryByGraphQlParameters(sbuilder.Parameters) []GenerationResult {
	return a.QueryResult
}

// Парсинг ответа от сервера в валидный результат
func (a AggregationServiceMock) ParseResult(sbuilder.Parameters, *elastic.AggregationBucketKeyItem) *AggregationResponse {
	return a.Parse
}
