package elastic_repo

import "bitbucket.org/sveshnikovwork/configurable-logger"

// Реализация (подставка) сервиса для упрощенного управления операциями в логах
type LoggerOperationIncreaseServiceMock struct {
	IsCalled bool
}

// Инкрементное увеличение уровня операций и создание новой
func (l *LoggerOperationIncreaseServiceMock) IncreaseOperations(o []configurable_logger.LogOperation) []configurable_logger.LogOperation {
	l.IsCalled = true
	return o
}
