package fieldMapper

import "github.com/olivere/elastic/v7"

// Преобразует переданное название поля в реальное значение, если такое есть.
type FieldMapperInterface interface {
	// Получение корректного названия поля
	Map(field string) string

	// Парсинг результатов поиска. Подменяет поля на имеющиеся в маппинге и возвращает результат
	ParseSearchRequest(result *elastic.SearchResult) []map[string]interface{}

	// Парсинг результатов получения сущности. Подменяет поля на имеющиеся в маппинге и возвращает результат
	ParseGetRequest(result *elastic.GetResult) map[string]interface{}

	// Маппит сущность для вставки или обновления. Если в сущности присутствует ID,
	// то возвращает его. Если присутствуют системные поля, то удаляет их.
	MapEntityToInsertOrUpdate(
		entity map[string]interface{},
	) (mappedEntity map[string]interface{}, id string, err error)
}
