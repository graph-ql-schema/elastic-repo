package sort

import (
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/orderParser"
	"github.com/olivere/elastic/v7"
)

// Генератор параметров сортировки
type SortGeneratorInterface interface {
	// Генерация параметров сортировки
	Generate(order []orderParser.Order) []elastic.Sorter
}
