package converter

import (
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

// Конвертер простых значений
type simpleProcessor struct {
	object    *graphql.Object
	converter gql_sql_converter.GraphQlSqlConverterInterface
}

// Проверка доступности процессора
func (s simpleProcessor) isAvailable(string) bool {
	return true
}

// Конвертация значения
func (s simpleProcessor) convert(field string, val interface{}) (interface{}, error) {
	return s.converter.ToBaseType(s.object, field, val)
}

// Фабрика процессора
func newSimpleProcessor(object *graphql.Object) converterProcessorInterface {
	return &simpleProcessor{
		object:    object,
		converter: gql_sql_converter.NewGraphQlSqlConverter(),
	}
}
