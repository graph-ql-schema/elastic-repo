package converter

import (
	"reflect"
	"testing"

	gql_root_type_getter "bitbucket.org/graph-ql-schema/gql-root-type-getter"
	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

func Test_listProcessor_isAvailable(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.NewList(graphql.Int),
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.String,
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		object     *graphql.Object
		converter  gql_sql_converter.GraphQlSqlConverterInterface
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование доступности",
			fields: fields{
				object:    objectConfig,
				converter: graphQlSqlConverterMock{},
				typeGetter: graphQlRootTypeGetterMock{
					List: true,
				},
			},
			args: args{
				field: "field_1",
			},
			want: true,
		},
		{
			name: "Тестирование доступности",
			fields: fields{
				object:    objectConfig,
				converter: graphQlSqlConverterMock{},
				typeGetter: graphQlRootTypeGetterMock{
					List: false,
				},
			},
			args: args{
				field: "field_2",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := listProcessor{
				object:     tt.fields.object,
				converter:  tt.fields.converter,
				typeGetter: tt.fields.typeGetter,
			}
			if got := l.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации значений
func Test_listProcessor_convert(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.NewList(graphql.Int),
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.String,
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		object     *graphql.Object
		converter  gql_sql_converter.GraphQlSqlConverterInterface
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field string
		val   interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование конвертации. Ошибка конвертации значения.",
			fields: fields{
				object: objectConfig,
				converter: graphQlSqlConverterMock{
					Result:  "test",
					IsError: true,
				},
				typeGetter: graphQlRootTypeGetterMock{
					List: true,
				},
			},
			args: args{
				field: "field_1",
				val:   []string{"test", "test"},
			},
			want:    []interface{}{},
			wantErr: true,
		},
		{
			name: "Тестирование конвертации. Все валидно.",
			fields: fields{
				object: objectConfig,
				converter: graphQlSqlConverterMock{
					Result:  "test_converted",
					IsError: false,
				},
				typeGetter: graphQlRootTypeGetterMock{
					List: true,
				},
			},
			args: args{
				field: "field_1",
				val:   []string{"test", "test"},
			},
			want:    []interface{}{"test_converted", "test_converted"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := listProcessor{
				object:     tt.fields.object,
				converter:  tt.fields.converter,
				typeGetter: tt.fields.typeGetter,
			}
			got, err := l.convert(tt.args.field, tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
