package groupBy

import (
	"encoding/json"
	"fmt"
	"sort"
	"sync"

	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Тип, описывающий результаты парсинга данных агрегации
type parseResult struct {
	priority int
	data     []*aggregation.AggregationResponse
}

// Сервис группировки для запросов агрегации
type groupByService struct {
	aggregationService aggregation.AggregationServiceInterface
	fieldMapper        fieldMapper.FieldMapperInterface
}

// Генерация запроса группировки
func (g groupByService) GenerateQuery(params sbuilder.Parameters) (code string, aggregation elastic.Aggregation) {
	groupBy := g.getGroupBy(params)
	field := groupBy[0]
	anotherFields := groupBy[1:]

	return g.getGroupByKey(field), g.recursiveGenerateQuery(params, field, anotherFields)
}

// Рекурсивная генерация вложенных группировок для запроса
func (g groupByService) recursiveGenerateQuery(
	params sbuilder.Parameters,
	field string,
	anotherFields []string,
) elastic.Aggregation {
	// Если это последний уровень вложенности группировок
	if 0 == len(anotherFields) {
		query := elastic.NewTermsAggregation().Field(g.fieldMapper.Map(field))
		for _, aggregationQuery := range g.aggregationService.GenerateQueryByGraphQlParameters(params) {
			query.SubAggregation(aggregationQuery.Name, aggregationQuery.Aggregation)
		}

		return query
	}

	// Генерируем дочерние вложенности для группировки для запроса
	nextField := anotherFields[0]
	nextLevelFields := anotherFields[1:]

	aggregationQuery := g.recursiveGenerateQuery(params, nextField, nextLevelFields)
	return elastic.NewTermsAggregation().
		Field(g.fieldMapper.Map(field)).
		SubAggregation(g.getGroupByKey(nextField), aggregationQuery)
}

// Парсинг результатов запроса агрегации
func (g groupByService) ParseResponse(
	params sbuilder.Parameters,
	response *elastic.SearchResult,
) []*aggregation.AggregationResponse {
	groupBy := g.getGroupBy(params)
	field := groupBy[0]
	anotherFields := groupBy[1:]

	aggregationData, ok := response.Aggregations[g.getGroupByKey(field)]
	if !ok {
		return []*aggregation.AggregationResponse{}
	}

	return g.parseResponseData(params, aggregationData, anotherFields)
}

// Парсинг данных агрегации на необходимом уровне
func (g groupByService) parseResponseData(
	params sbuilder.Parameters,
	aggregationData json.RawMessage,
	anotherFields []string,
) []*aggregation.AggregationResponse {
	var aggregationBucketKeyItems elastic.AggregationBucketKeyItems
	err := json.Unmarshal(aggregationData, &aggregationBucketKeyItems)
	if nil != err {
		return []*aggregation.AggregationResponse{}
	}

	// Если необходимо обрабатывать вложенные уровни группировки
	if 0 != len(anotherFields) {
		return g.parseResponseGroups(aggregationBucketKeyItems, params, anotherFields)
	}

	// Вложенных группировок нет
	return g.parseBuckets(aggregationBucketKeyItems, params)
}

// Парсинг подгрупп группировки запроса
func (g groupByService) parseResponseGroups(
	aggregationBucketKeyItems elastic.AggregationBucketKeyItems,
	params sbuilder.Parameters,
	anotherFields []string,
) []*aggregation.AggregationResponse {
	var wg sync.WaitGroup
	parseChan := make(chan parseResult)
	resultChan := make(chan []*aggregation.AggregationResponse)
	completeChan := make(chan bool)

	defer close(parseChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов парсинга
	go g.readResults(parseChan, resultChan, completeChan)

	// Запускаем асинхронную обработку бакетов
	wg.Add(len(aggregationBucketKeyItems.Buckets))
	for i, bucket := range aggregationBucketKeyItems.Buckets {
		go g.parseGroup(&wg, parseChan, i, anotherFields, bucket, params)
	}

	wg.Wait()
	completeChan <- true

	return <-resultChan
}

// Асинхронный парсинг подгруппы запроса
func (g groupByService) parseGroup(
	wg *sync.WaitGroup,
	parseChan chan parseResult,
	num int,
	anotherFields []string,
	bucket *elastic.AggregationBucketKeyItem,
	params sbuilder.Parameters,
) {
	defer wg.Done()

	field := anotherFields[0]
	nextFields := anotherFields[1:]

	aggregationData, ok := bucket.Aggregations[g.getGroupByKey(field)]
	if !ok {
		parseChan <- parseResult{
			priority: num,
			data:     []*aggregation.AggregationResponse{},
		}

		return
	}

	parseChan <- parseResult{
		priority: num,
		data:     g.parseResponseData(params, aggregationData, nextFields),
	}
}

// Парсинг бакетов агрегации
func (g groupByService) parseBuckets(
	items elastic.AggregationBucketKeyItems,
	params sbuilder.Parameters,
) []*aggregation.AggregationResponse {
	var wg sync.WaitGroup
	parseChan := make(chan parseResult)
	resultChan := make(chan []*aggregation.AggregationResponse)
	completeChan := make(chan bool)

	defer close(parseChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов парсинга
	go g.readResults(parseChan, resultChan, completeChan)

	// Запускаем асинхронную обработку бакетов
	wg.Add(len(items.Buckets))
	for i, bucket := range items.Buckets {
		go g.parseBucket(&wg, parseChan, i, bucket, params)
	}

	wg.Wait()
	completeChan <- true

	return <-resultChan
}

// Асинхронное чтение результатов парсинга бакетов
func (g groupByService) readResults(
	parseChan chan parseResult,
	resultChan chan []*aggregation.AggregationResponse,
	completeChan chan bool,
) {
	items := []parseResult{}
	for {
		select {
		case item := <-parseChan:
			items = append(items, item)
			break
		case _ = <-completeChan:
			sort.SliceStable(items, func(i, j int) bool {
				return items[i].priority < items[j].priority
			})

			result := []*aggregation.AggregationResponse{}
			for _, item := range items {
				for _, data := range item.data {
					result = append(result, data)
				}
			}

			resultChan <- result

			return
		}
	}
}

// Асинхронный парсинг одного бакета результата
func (g groupByService) parseBucket(
	wg *sync.WaitGroup,
	parseChan chan parseResult,
	num int,
	bucket *elastic.AggregationBucketKeyItem,
	params sbuilder.Parameters,
) {
	defer wg.Done()

	parseChan <- parseResult{
		priority: num,
		data: []*aggregation.AggregationResponse{
			g.aggregationService.ParseResult(params, bucket),
		},
	}
}

// Генерация ключа группировки
func (g groupByService) getGroupByKey(field string) string {
	return fmt.Sprintf(`%v_group_by`, field)
}

// Получение полей группировки из переданных параметров
func (g groupByService) getGroupBy(params sbuilder.Parameters) []string {
	groupBy := []string{"_index"}
	if 0 != len(params.Arguments.GroupBy) {
		groupBy = params.Arguments.GroupBy
	}

	return groupBy
}

// Фабрика сервиса
func NewGroupByService(object *graphql.Object, fieldsMap map[string]string) GroupByServiceInterface {
	return &groupByService{
		aggregationService: aggregation.NewAggregationService(object, fieldsMap),
		fieldMapper:        fieldMapper.NewFieldMapper(object, fieldsMap),
	}
}
