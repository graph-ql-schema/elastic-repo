package filter

import (
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Генератор параметров фильтрации
type QueryGeneratorInterface interface {
	// Генерация эквивалента переданного запроса фильтрации для ElasticSearch
	Generate(params whereOrHavingParser.Operation) (elastic.Query, error)
}

// Процессор для генерации параметров фильтрации
type queryGeneratorProcessorInterface interface {
	// Проверка доступности процессора
	isAvailable(params whereOrHavingParser.Operation) bool

	// Генерация запроса
	generate(params whereOrHavingParser.Operation) (elastic.Query, error)
}

// Обработчик множественных сущностей. Использует под капотом распараллеливание обработки.
type multiProcessorInterface interface {
	// Установка генератора для парсинга
	setGenerator(generator QueryGeneratorInterface)

	// Обработка переданных сущностей
	process(params []whereOrHavingParser.Operation) ([]elastic.Query, error)
}
