package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Генератор запросов AND
type andQueryGeneratorProcessor struct {
	multiProcessor multiProcessorInterface
}

// Проверка доступности процессора
func (a andQueryGeneratorProcessor) isAvailable(params whereOrHavingParser.Operation) bool {
	return params.Type() == constants.AndSchemaKey ||
		params.Type() == constants.WhereSchemaKey ||
		params.Type() == constants.HavingSchemaKey
}

// Генерация запроса
func (a andQueryGeneratorProcessor) generate(params whereOrHavingParser.Operation) (elastic.Query, error) {
	fields, err := a.multiProcessor.process(params.Value().([]whereOrHavingParser.Operation))
	if nil != err {
		return nil, fmt.Errorf(`AND operation: %v`, err.Error())
	}

	return elastic.NewBoolQuery().Must(fields...), nil
}

// Фабрика процессора
func newAndQueryGeneratorProcessor(multiProcessor multiProcessorInterface) queryGeneratorProcessorInterface {
	return &andQueryGeneratorProcessor{
		multiProcessor: multiProcessor,
	}
}
