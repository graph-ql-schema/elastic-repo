package filter

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование генерации
func TestQueryGenerator_Generate(t *testing.T) {
	type fields struct {
		processors []queryGeneratorProcessorInterface
	}
	type args struct {
		params whereOrHavingParser.Operation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    elastic.Query
		wantErr bool
	}{
		{
			name: "Тестирование генерации без доступных процессоров",
			fields: fields{
				processors: []queryGeneratorProcessorInterface{
					processorMock{
						IsAvailable: false,
						IsError:     false,
						Result:      nil,
					},
				},
			},
			args: args{
				params: operationMock{
					rType:  "test",
					rValue: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации с возвратом ошибки процессора",
			fields: fields{
				processors: []queryGeneratorProcessorInterface{
					processorMock{
						IsAvailable: true,
						IsError:     true,
						Result:      nil,
					},
				},
			},
			args: args{
				params: operationMock{
					rType:  "test",
					rValue: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации без ошибки процессора",
			fields: fields{
				processors: []queryGeneratorProcessorInterface{
					processorMock{
						IsAvailable: true,
						IsError:     false,
						Result:      elastic.NewTermQuery("test", "val"),
					},
				},
			},
			args: args{
				params: operationMock{
					rType:  "test",
					rValue: "test",
				},
			},
			want:    elastic.NewTermQuery("test", "val"),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				processors: tt.fields.processors,
			}
			got, err := q.Generate(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("Generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
