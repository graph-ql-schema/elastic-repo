package aggregation

import (
	"encoding/json"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/converter"
	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/requestedFieldsParser"
	"github.com/olivere/elastic/v7"
)

func Test_variantsProcessor_isGenerationAvailable(t *testing.T) {
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование на варианте с доступной операцией",
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
			},
			want: true,
		},
		{
			name: "Тестирование на варианте без доступной операции",
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.AvgOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variantsProcessor{}
			if got := v.isGenerationAvailable(tt.args.params); got != tt.want {
				t.Errorf("isGenerationAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса
func Test_variantsProcessor_generateQuery(t *testing.T) {
	type fields struct {
		fieldMapper fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []GenerationResult
	}{
		{
			name: "Тестирование генерации с одним валидным полем",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "name_aggregation_operation_variants",
					Aggregation: elastic.NewTermsAggregation().Field("name"),
				},
			},
		},
		{
			name: "Тестирование генерации с несколькими валидными полями",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
								requestedFieldsParser.NewGraphQlRequestedField("surname", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "name_aggregation_operation_variants",
					Aggregation: elastic.NewTermsAggregation().Field("name"),
				},
				{
					Name:        "surname_aggregation_operation_variants",
					Aggregation: elastic.NewTermsAggregation().Field("surname"),
				},
			},
		},
		{
			name: "Тестирование генерации с несколькими полями",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.AvgOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "name_aggregation_operation_variants",
					Aggregation: elastic.NewTermsAggregation().Field("name"),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variantsProcessor{
				fieldMapper: tt.fields.fieldMapper,
			}
			if got := v.generateQuery(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generateQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга результатов
func Test_variantsProcessor_parseResult(t *testing.T) {
	rawJsonGenerator := func(values ...interface{}) json.RawMessage {
		valuesData := []map[string]interface{}{}
		for _, value := range values {
			valuesData = append(valuesData, map[string]interface{}{
				"key":       value,
				"doc_count": 1,
			})
		}

		data, _ := json.Marshal(map[string]interface{}{
			"doc_count_error_upper_bound": 0,
			"sum_other_doc_count":         0,
			"buckets":                     valuesData,
		})

		return data
	}

	type fields struct {
		fieldMapper fieldMapper.FieldMapperInterface
		converter   converter.ConverterInterface
	}
	type args struct {
		params sbuilder.Parameters
		result *AggregationResponse
		data   *elastic.AggregationBucketKeyItem
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   AggregationResponse
	}{
		{
			name: "Тестирование парсинга с несколькими значениями варианта с ошибкой конвертации",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
				converter: converterMock{
					IsError: true,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"name_aggregation_operation_variants": rawJsonGenerator("test-1", "test-2"),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max:   nil,
				Variants: map[string]Variants{
					"name": {},
				},
			},
		},
		{
			name: "Тестирование парсинга с несколькими значениями варианта без ошибки конвертации",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
				converter: converterMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"name_aggregation_operation_variants": rawJsonGenerator("test-1", "test-2"),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max:   nil,
				Variants: map[string]Variants{
					"name": {
						"test-1",
						"test-2",
					},
				},
			},
		},
		{
			name: "Тестирование парсинга с несколькими значениями нескольких вариантов без ошибки конвертации",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
				converter: converterMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
								requestedFieldsParser.NewGraphQlRequestedField("surname", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"name_aggregation_operation_variants":    rawJsonGenerator("test-1", "test-2"),
						"surname_aggregation_operation_variants": rawJsonGenerator("test-3", "test-4", "test-5"),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max:   nil,
				Variants: map[string]Variants{
					"name": {
						"test-1",
						"test-2",
					},
					"surname": {
						"test-3",
						"test-4",
						"test-5",
					},
				},
			},
		},
		{
			name: "Тестирование парсинга с несколькими значениями варианта без ошибки конвертации со сторонними данными других агрегаций",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
				converter: converterMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.AvgOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"name_aggregation_operation_variants": rawJsonGenerator("test-1", "test-2"),
						"price_aggregation_operation_avg":     rawJsonGenerator(10),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max:   nil,
				Variants: map[string]Variants{
					"name": {
						"test-1",
						"test-2",
					},
				},
			},
		},
		{
			name: "Тестирование парсинга нескольких вариантов без ошибки конвертации, где у одного варианта нет значений",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
				converter: converterMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
								requestedFieldsParser.NewGraphQlRequestedField("surname", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"name_aggregation_operation_variants": rawJsonGenerator("test-1", "test-2"),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max:   nil,
				Variants: map[string]Variants{
					"name": {
						"test-1",
						"test-2",
					},
					"surname": {},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variantsProcessor{
				fieldMapper: tt.fields.fieldMapper,
				converter:   tt.fields.converter,
			}

			if v.parseResult(tt.args.params, tt.args.result, tt.args.data); !reflect.DeepEqual(*tt.args.result, tt.want) {
				t.Errorf("parseResult() = %v, want %v", *tt.args.result, tt.want)
			}
		})
	}
}
