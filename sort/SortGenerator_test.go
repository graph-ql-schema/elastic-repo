package sort

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/orderParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование генерации
func Test_sortGenerator_Generate(t *testing.T) {
	type fields struct {
		fieldsMapper fieldMapper.FieldMapperInterface
	}
	type args struct {
		order []orderParser.Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []elastic.Sorter
	}{
		{
			name: "Тестирование без передачи параметров",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{},
			want: nil,
		},
		{
			name: "Тестирование с передачей одного параметра",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				order: []orderParser.Order{
					{
						Priority:  1,
						By:        "test",
						Direction: "desc",
					},
				},
			},
			want: []elastic.Sorter{
				elastic.NewFieldSort("test").Desc(),
			},
		},
		{
			name: "Тестирование с передачей двух параметров",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				order: []orderParser.Order{
					{
						Priority:  1,
						By:        "test",
						Direction: "desc",
					},
					{
						Priority:  2,
						By:        "fest",
						Direction: "asc",
					},
				},
			},
			want: []elastic.Sorter{
				elastic.NewFieldSort("test").Desc(),
				elastic.NewFieldSort("fest").Asc(),
			},
		},
		{
			name: "Тестирование с передачей двух параметров с нарушением порядка",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				order: []orderParser.Order{
					{
						Priority:  2,
						By:        "fest",
						Direction: "asc",
					},
					{
						Priority:  1,
						By:        "test",
						Direction: "desc",
					},
				},
			},
			want: []elastic.Sorter{
				elastic.NewFieldSort("test").Desc(),
				elastic.NewFieldSort("fest").Asc(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sortGenerator{
				fieldsMapper: tt.fields.fieldsMapper,
			}
			if got := s.Generate(tt.args.order); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
