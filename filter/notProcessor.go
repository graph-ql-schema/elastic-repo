package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Генератор операции NOT
type notProcessor struct {
	multiProcessor multiProcessorInterface
}

// Проверка доступности процессора
func (n notProcessor) isAvailable(params whereOrHavingParser.Operation) bool {
	return params.Type() == constants.NotSchemaKey
}

// Генерация запроса
func (n notProcessor) generate(params whereOrHavingParser.Operation) (elastic.Query, error) {
	fields, err := n.multiProcessor.process(params.Value().([]whereOrHavingParser.Operation))
	if nil != err {
		return nil, fmt.Errorf(`NOT operation: %v`, err.Error())
	}

	return elastic.NewBoolQuery().MustNot(fields...), nil
}

// Фабрика процессора
func newNotProcessor(multiProcessor multiProcessorInterface) queryGeneratorProcessorInterface {
	return &notProcessor{
		multiProcessor: multiProcessor,
	}
}
