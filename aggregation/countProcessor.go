package aggregation

import (
	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/olivere/elastic/v7"
)

// Процессор запроса количества элементов
type countProcessor struct{}

// Процессор не требует особых условий для генерации
func (c countProcessor) isGenerationAvailable(sbuilder.Parameters) bool {
	return false
}

// Не должно быть специальной генерации подзапроса для вычисления количества
func (c countProcessor) generateQuery(sbuilder.Parameters) []GenerationResult {
	return nil
}

// Парсинг результатов
func (c countProcessor) parseResult(
	params sbuilder.Parameters,
	result *AggregationResponse,
	data *elastic.AggregationBucketKeyItem,
) {
	result.Count = data.DocCount
}

// Фабрика процессора
func newCountProcessor() aggregationProcessorInterface {
	return &countProcessor{}
}
