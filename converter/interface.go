package converter

// Конвертер значений, полученных из БД в валидный для GraphQL формат
type ConverterInterface interface {
	// Конвертация значений
	Convert(field string, val interface{}) (interface{}, error)
}

// Процессор конвертации значений
type converterProcessorInterface interface {
	// Проверка доступности процессора
	isAvailable(field string) bool

	// Конвертация значения
	convert(field string, val interface{}) (interface{}, error)
}
