package aggregation

import (
	"encoding/json"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/requestedFieldsParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование доступности генератора
func Test_mathProcessor_isGenerationAvailable(t *testing.T) {
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование на варианте с доступной операцией",
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.AvgOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
			},
			want: true,
		},
		{
			name: "Тестирование на варианте без доступной операции",
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mathProcessor{}
			if got := m.isGenerationAvailable(tt.args.params); got != tt.want {
				t.Errorf("isGenerationAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса
func Test_mathProcessor_generateQuery1(t *testing.T) {
	type fields struct {
		fieldMapper fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []GenerationResult
	}{
		{
			name: "Тестирование генерации AVG",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.AvgOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "price_aggregation_operation_avg",
					Aggregation: elastic.NewAvgAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_avg",
					Aggregation: elastic.NewAvgAggregation().Field("qtty"),
				},
			},
		},
		{
			name: "Тестирование генерации SUM",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.SumOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "price_aggregation_operation_sum",
					Aggregation: elastic.NewSumAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_sum",
					Aggregation: elastic.NewSumAggregation().Field("qtty"),
				},
			},
		},
		{
			name: "Тестирование генерации MIN",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MinOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "price_aggregation_operation_min",
					Aggregation: elastic.NewMinAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_min",
					Aggregation: elastic.NewMinAggregation().Field("qtty"),
				},
			},
		},
		{
			name: "Тестирование генерации MAX",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "price_aggregation_operation_max",
					Aggregation: elastic.NewMaxAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_max",
					Aggregation: elastic.NewMaxAggregation().Field("qtty"),
				},
			},
		},
		{
			name: "Тестирование совместной генерации с 2 валидными полями",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MinOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "price_aggregation_operation_min",
					Aggregation: elastic.NewMinAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_min",
					Aggregation: elastic.NewMinAggregation().Field("qtty"),
				},
				{
					Name:        "price_aggregation_operation_max",
					Aggregation: elastic.NewMaxAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_max",
					Aggregation: elastic.NewMaxAggregation().Field("qtty"),
				},
			},
		},
		{
			name: "Тестирование совместной генерации с 2 полями, из которых валидно только одно",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.VariantsOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("name", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "price_aggregation_operation_max",
					Aggregation: elastic.NewMaxAggregation().Field("price"),
				},
				{
					Name:        "qtty_aggregation_operation_max",
					Aggregation: elastic.NewMaxAggregation().Field("qtty"),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mathProcessor{
				fieldMapper: tt.fields.fieldMapper,
			}
			if got := m.generateQuery(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generateQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга результата
func Test_mathProcessor_parseResult(t *testing.T) {
	rawJsonGenerator := func(value float64) json.RawMessage {
		data, _ := json.Marshal(map[string]interface{}{
			"value": value,
		})

		return data
	}

	type args struct {
		params sbuilder.Parameters
		result *AggregationResponse
		data   *elastic.AggregationBucketKeyItem
	}
	type fields struct {
		fieldMapper fieldMapper.FieldMapperInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   AggregationResponse
	}{
		{
			name: "Тестирование парсинга AVG агрегаций",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.AvgOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"price_aggregation_operation_avg": rawJsonGenerator(10),
						"qtty_aggregation_operation_avg":  rawJsonGenerator(15),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg: map[string]float64{
					"price": 10,
					"qtty":  15,
				},
				Sum:      nil,
				Min:      nil,
				Max:      nil,
				Variants: nil,
			},
		},
		{
			name: "Тестирование парсинга SUM агрегаций",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.SumOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"price_aggregation_operation_sum": rawJsonGenerator(10),
						"qtty_aggregation_operation_sum":  rawJsonGenerator(15),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum: map[string]float64{
					"price": 10,
					"qtty":  15,
				},
				Min:      nil,
				Max:      nil,
				Variants: nil,
			},
		},
		{
			name: "Тестирование парсинга MIN агрегаций",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MinOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"price_aggregation_operation_min": rawJsonGenerator(10),
						"qtty_aggregation_operation_min":  rawJsonGenerator(15),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min: map[string]float64{
					"price": 10,
					"qtty":  15,
				},
				Max:      nil,
				Variants: nil,
			},
		},
		{
			name: "Тестирование парсинга MAX агрегаций",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"price_aggregation_operation_max": rawJsonGenerator(10),
						"qtty_aggregation_operation_max":  rawJsonGenerator(15),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max: map[string]float64{
					"price": 10,
					"qtty":  15,
				},
				Variants: nil,
			},
		},
		{
			name: "Тестирование парсинга совместных агрегаций",
			fields: fields{
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MinOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("price", nil),
							},
						),
					},
				},
				result: &AggregationResponse{},
				data: &elastic.AggregationBucketKeyItem{
					Aggregations: map[string]json.RawMessage{
						"price_aggregation_operation_min": rawJsonGenerator(10),
						"qtty_aggregation_operation_max":  rawJsonGenerator(15),
					},
				},
			},
			want: AggregationResponse{
				Count: 0,
				Avg:   nil,
				Sum:   nil,
				Min: map[string]float64{
					"price": 10,
				},
				Max: map[string]float64{
					"qtty": 15,
				},
				Variants: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mathProcessor{
				fieldMapper: tt.fields.fieldMapper,
			}
			if m.parseResult(tt.args.params, tt.args.result, tt.args.data); !reflect.DeepEqual(*tt.args.result, tt.want) {
				t.Errorf("parseResult() = %v, want %v", *tt.args.result, tt.want)
			}
		})
	}
}
