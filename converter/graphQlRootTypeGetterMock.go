package converter

import (
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type graphQlRootTypeGetterMock struct {
	RootType graphql.Type
	NotNull  bool
	List     bool
}

// Получение корневого типа поля, которое обернут в NotNull или в List
func (g graphQlRootTypeGetterMock) GetRootType(graphql.Type) graphql.Type {
	return g.RootType
}

// Проверяет, что переданный тип является NotNull
func (g graphQlRootTypeGetterMock) IsNotNull(graphql.Type) bool {
	return g.NotNull
}

// Проверяет, что переданный тип является List
func (g graphQlRootTypeGetterMock) IsList(graphql.Type) bool {
	return g.List
}
