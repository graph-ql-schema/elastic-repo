package queries

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/elastic-repo/filter"
	"bitbucket.org/graph-ql-schema/elastic-repo/sort"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Генератор запросов листинга сущностей
type listQueryBuilder struct {
	logger        configurable_logger.LoggerInterface
	logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
	queryBuilder  filter.QueryGeneratorInterface
	sortBuilder   sort.SortGeneratorInterface
	mapper        fieldMapper.FieldMapperInterface
}

// Парсинг ответа на запрос
func (l listQueryBuilder) Parse(
	response *elastic.SearchResult,
	operations []configurable_logger.LogOperation,
) []map[string]interface{} {
	ops := l.logIncService.IncreaseOperations(operations)

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	result := l.mapper.ParseSearchRequest(response)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	l.logger.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Parsed search request result. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"result": result,
		},
		Operations: ops,
	})

	return result
}

// Построение запроса листинга сущностей
func (l listQueryBuilder) Build(
	params sbuilder.Parameters,
	operations []configurable_logger.LogOperation,
) (*elastic.SearchSource, error) {
	startsGeneration := time.Now().UnixNano() / int64(time.Millisecond)
	ops := l.logIncService.IncreaseOperations(operations)
	result, err := l.parseWhereParameters(elastic.NewSearchSource(), params, ops)
	if nil != err {
		return nil, err
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	orders := l.sortBuilder.Generate(params.Arguments.OrderBy)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	l.logger.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Built sort parameters. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"orderBy": params.Arguments.OrderBy,
			"orders":  orders,
		},
		Operations: ops,
	})

	if nil != orders {
		result = result.SortBy(orders...)
	}

	result = result.
		From(int(params.Arguments.Pagination.Offset)).
		Size(int(params.Arguments.Pagination.Limit))

	source, _ := result.Source()
	jsonSource, _ := json.Marshal(source)
	finish = time.Now().UnixNano() / int64(time.Millisecond)

	l.logger.Debug(configurable_logger.LogParameters{
		Code:    200,
		Message: fmt.Sprintf(`Generated list query. Execution time: %v ms`, finish-startsGeneration),
		Data: map[string]interface{}{
			"query": string(jsonSource),
			"params": map[string]interface{}{
				"arguments": params.Arguments,
				"fields":    params.Fields,
			},
		},
		Operations: ops,
	})

	return result, nil
}

// Парсинг параметров Where запроса
func (l listQueryBuilder) parseWhereParameters(
	source *elastic.SearchSource,
	params sbuilder.Parameters,
	operations []configurable_logger.LogOperation,
) (*elastic.SearchSource, error) {
	if nil == params.Arguments.Where {
		return source, nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query, err := l.queryBuilder.Generate(params.Arguments.Where)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		err = fmt.Errorf(`filter parsing error: %v`, err.Error())
		l.logger.Warning(configurable_logger.LogParameters{
			Code:    400,
			Message: fmt.Sprintf(`Failed to parse filter request: %v`, err.Error()),
			Data: map[string]interface{}{
				"params": map[string]interface{}{
					"arguments": params.Arguments,
					"fields":    params.Fields,
				},
				"err": err,
			},
			Operations: operations,
		})

		return nil, err
	}

	l.logger.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Built filter query. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"params": map[string]interface{}{
				"arguments": params.Arguments,
				"fields":    params.Fields,
			},
		},
		Operations: operations,
	})

	return source.Query(query), nil
}

// Фабрика генератора запроса
func NewListQueryBuilder(
	object *graphql.Object,
	fieldsMap map[string]string,
	factory configurable_logger.TLoggerFactory,
) ListQueryBuilderInterface {
	return &listQueryBuilder{
		logger:        factory(`listQueryBuilder`),
		logIncService: configurable_logger.LoggerOperationIncreaseService(),
		queryBuilder:  filter.NewQueryGenerator(object, fieldsMap),
		sortBuilder:   sort.NewSortGenerator(object, fieldsMap),
		mapper:        fieldMapper.NewFieldMapper(object, fieldsMap),
	}
}
