package aggregation

import "fmt"

// Подставка для тестирования
type converterMock struct {
	IsError bool
}

// Конвертация значений
func (c converterMock) Convert(field string, val interface{}) (interface{}, error) {
	if c.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return val, nil
}
