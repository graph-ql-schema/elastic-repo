package sort

import (
	"sort"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/orderParser"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Генератор параметров сортировки
type sortGenerator struct {
	fieldsMapper fieldMapper.FieldMapperInterface
}

// Генерация параметров сортировки
func (s sortGenerator) Generate(order []orderParser.Order) []elastic.Sorter {
	if 0 == len(order) {
		return nil
	}

	sort.SliceStable(order, func(i, j int) bool {
		return order[i].Priority < order[j].Priority
	})

	orders := []elastic.Sorter{}
	for _, order := range order {
		field := s.fieldsMapper.Map(order.By)
		orderObj := elastic.NewFieldSort(field)

		if order.Direction == "desc" {
			orderObj = orderObj.Desc()
		} else {
			orderObj = orderObj.Asc()
		}

		orders = append(orders, orderObj)
	}

	return orders
}

// Фабрика генерации
func NewSortGenerator(object *graphql.Object, fieldsMap map[string]string) SortGeneratorInterface {
	return &sortGenerator{
		fieldsMapper: fieldMapper.NewFieldMapper(object, fieldsMap),
	}
}
