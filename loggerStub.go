package elastic_repo

import (
	"bitbucket.org/sveshnikovwork/configurable-logger"
)

// Заглушка для тестирования сервиса
type LoggerStubForTest struct{}

// Логирование на уровне Notice
func (l LoggerStubForTest) Notice(configurable_logger.LogParameters) {}

// Логирование на уровне Info
func (l LoggerStubForTest) Info(configurable_logger.LogParameters) {}

// Логирование отладочной информации
func (l LoggerStubForTest) Debug(configurable_logger.LogParameters) {}

// Логирование предупреждений
func (l LoggerStubForTest) Warning(configurable_logger.LogParameters) {}

// Логирование ошибок
func (l LoggerStubForTest) Error(configurable_logger.LogParameters) {}
