package groupBy

import (
	"encoding/json"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование генерации запроса
func Test_groupByService_GenerateQuery(t *testing.T) {
	type fields struct {
		aggregationService aggregation.AggregationServiceInterface
		fieldMapper        fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name            string
		fields          fields
		args            args
		wantCode        string
		wantAggregation elastic.Aggregation
	}{
		{
			name: "Тестирование генерации группировки на одном поле",
			fields: fields{
				aggregationService: aggregation.AggregationServiceMock{
					QueryResult: []aggregation.GenerationResult{
						{
							Name:        "test_aggregation",
							Aggregation: elastic.NewSumAggregation().Field("price"),
						},
					},
					Parse: nil,
				},
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: []string{"name"},
					},
				},
			},
			wantCode: "name_group_by",
			wantAggregation: elastic.NewTermsAggregation().
				Field("name").
				SubAggregation("test_aggregation", elastic.NewSumAggregation().Field("price")),
		},
		{
			name: "Тестирование генерации группировки без полей",
			fields: fields{
				aggregationService: aggregation.AggregationServiceMock{
					QueryResult: []aggregation.GenerationResult{
						{
							Name:        "test_aggregation",
							Aggregation: elastic.NewSumAggregation().Field("price"),
						},
					},
					Parse: nil,
				},
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: nil,
					},
				},
			},
			wantCode: "_index_group_by",
			wantAggregation: elastic.NewTermsAggregation().
				Field("_index").
				SubAggregation("test_aggregation", elastic.NewSumAggregation().Field("price")),
		},
		{
			name: "Тестирование генерации группировки на двух полях",
			fields: fields{
				aggregationService: aggregation.AggregationServiceMock{
					QueryResult: []aggregation.GenerationResult{
						{
							Name:        "test_aggregation",
							Aggregation: elastic.NewSumAggregation().Field("price"),
						},
					},
					Parse: nil,
				},
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: []string{"name", "surname"},
					},
				},
			},
			wantCode: "name_group_by",
			wantAggregation: elastic.NewTermsAggregation().
				Field("name").
				SubAggregation(
					"surname_group_by",
					elastic.NewTermsAggregation().
						Field("surname").
						SubAggregation("test_aggregation", elastic.NewSumAggregation().Field("price")),
				),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByService{
				aggregationService: tt.fields.aggregationService,
				fieldMapper:        tt.fields.fieldMapper,
			}
			gotCode, gotAggregation := g.GenerateQuery(tt.args.params)
			if gotCode != tt.wantCode {
				t.Errorf("GenerateQuery() gotCode = %v, want %v", gotCode, tt.wantCode)
			}
			if !reflect.DeepEqual(gotAggregation, tt.wantAggregation) {
				t.Errorf("GenerateQuery() gotAggregation = %v, want %v", gotAggregation, tt.wantAggregation)
			}
		})
	}
}

// Тестирование парсинга результатов запроса
func Test_groupByService_ParseResponse(t *testing.T) {
	rawMessage := func(data interface{}) json.RawMessage {
		res, _ := json.Marshal(data)

		return res
	}

	type fields struct {
		aggregationService aggregation.AggregationServiceInterface
		fieldMapper        fieldMapper.FieldMapperInterface
	}
	type args struct {
		params   sbuilder.Parameters
		response *elastic.SearchResult
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []*aggregation.AggregationResponse
	}{
		{
			name: "Тестирование генерации группировки на одном поле",
			fields: fields{
				aggregationService: aggregation.AggregationServiceMock{
					Parse: &aggregation.AggregationResponse{
						Count: 1,
					},
				},
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: []string{"name"},
					},
				},
				response: &elastic.SearchResult{
					Aggregations: map[string]json.RawMessage{
						"name_group_by": rawMessage(map[string]interface{}{
							"doc_count_error_upper_bound": 0,
							"sum_other_doc_count":         0,
							"buckets": []map[string]interface{}{
								{
									"key":       "name_group_by_data",
									"doc_count": 3,
								},
							},
						}),
					},
				},
			},
			want: []*aggregation.AggregationResponse{
				{
					Count: 1,
				},
			},
		},
		{
			name: "Тестирование генерации группировки без полей",
			fields: fields{
				aggregationService: aggregation.AggregationServiceMock{
					Parse: &aggregation.AggregationResponse{
						Count: 1,
					},
				},
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: []string{},
					},
				},
				response: &elastic.SearchResult{
					Aggregations: map[string]json.RawMessage{
						"_index_group_by": rawMessage(map[string]interface{}{
							"doc_count_error_upper_bound": 0,
							"sum_other_doc_count":         0,
							"buckets": []map[string]interface{}{
								{
									"key":       "_index_group_by_data",
									"doc_count": 3,
								},
							},
						}),
					},
				},
			},
			want: []*aggregation.AggregationResponse{
				{
					Count: 1,
				},
			},
		},
		{
			name: "Тестирование генерации группировки на двух полях",
			fields: fields{
				aggregationService: aggregation.AggregationServiceMock{
					Parse: &aggregation.AggregationResponse{
						Count: 1,
					},
				},
				fieldMapper: fieldMapper.FieldMapperMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: []string{"name", "surname"},
					},
				},
				response: &elastic.SearchResult{
					Aggregations: map[string]json.RawMessage{
						"name_group_by": rawMessage(map[string]interface{}{
							"doc_count_error_upper_bound": 0,
							"sum_other_doc_count":         0,
							"buckets": []map[string]interface{}{
								{
									"key":       "name_group_by_data",
									"doc_count": 3,
									"surname_group_by": map[string]interface{}{
										"doc_count_error_upper_bound": 0,
										"sum_other_doc_count":         0,
										"buckets": []map[string]interface{}{
											{
												"key":       "surname_group_by_data",
												"doc_count": 3,
											},
										},
									},
								},
								{
									"key":       "name_group_by_data",
									"doc_count": 3,
									"surname_group_by": map[string]interface{}{
										"doc_count_error_upper_bound": 0,
										"sum_other_doc_count":         0,
										"buckets": []map[string]interface{}{
											{
												"key":       "surname_group_by_data",
												"doc_count": 3,
											},
										},
									},
								},
							},
						}),
					},
				},
			},
			want: []*aggregation.AggregationResponse{
				{
					Count: 1,
				},
				{
					Count: 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByService{
				aggregationService: tt.fields.aggregationService,
				fieldMapper:        tt.fields.fieldMapper,
			}
			if got := g.ParseResponse(tt.args.params, tt.args.response); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseResponse() = %v, want %v", got, tt.want)
			}
		})
	}
}
