package converter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Конвертер значений, полученных из БД в валидный для GraphQL формат
type converter struct {
	processors []converterProcessorInterface
}

// Конвертация значений
func (c converter) Convert(field string, val interface{}) (interface{}, error) {
	for _, proc := range c.processors {
		if proc.isAvailable(field) {
			return proc.convert(field, val)
		}
	}

	return nil, fmt.Errorf(`no available processor`)
}

// Фабрика конвертера
func NewConverter(object *graphql.Object) ConverterInterface {
	return &converter{
		processors: []converterProcessorInterface{
			newListProcessor(object),
			newDateProcessor(object),
			newSimpleProcessor(object),
		},
	}
}
