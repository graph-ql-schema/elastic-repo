package main

import "github.com/graphql-go/graphql"

var testObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "test",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.ID,
				Name: "id",
			},
			"name": &graphql.Field{
				Type: graphql.String,
				Name: "name",
			},
			"operations": &graphql.Field{
				Type: graphql.NewList(graphql.String),
				Name: "operations",
			},
			"created": &graphql.Field{
				Type: graphql.DateTime,
				Name: "created",
			},
			"num": &graphql.Field{
				Type: graphql.Int,
				Name: "num",
			},
			"flt": &graphql.Field{
				Type: graphql.Float,
				Name: "flt",
			},
			"active": &graphql.Field{
				Type: graphql.Boolean,
				Name: "active",
			},
		},
		Description: "test entity",
	},
)

var fieldsMap = map[string]string{
	"id": "_id",
}
