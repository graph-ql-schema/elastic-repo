package filter

import (
	"github.com/graphql-go/graphql"
)

// Заглушка для тестирования
type operationMock struct {
	rType  string
	rValue interface{}
}

// Получение типа текущей операции
func (o operationMock) Type() string {
	return o.rType
}

// Получение значения для текущей операции
func (o operationMock) Value() interface{} {
	return o.rValue
}

// Получение кода поля для операции
func (o operationMock) Field() string {
	return "test_field"
}

// Конвертация в SQL
func (o operationMock) ToSQL(*graphql.Object, map[string]string) (string, error) {
	return "", nil
}
