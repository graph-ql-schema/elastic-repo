package elastic_repo

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/elastic-repo/executor"
	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/elastic-repo/queries"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	configurable_logger "bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Тестирование получения данных агрегации
func Test_elasticSearchGraphQlRepository_GetAggregationDataByRequest(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*aggregation.AggregationResponse
		wantErr bool
	}{
		{
			name: "Тестирование с возвращением ошибки при генерации запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{
					IsError:      true,
					IsParseError: false,
					Query:        elastic.NewSearchSource(),
					Result:       []*aggregation.AggregationResponse{},
				},
				listQuery:      queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper:         fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с возвращением ошибки при выполнении запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: true,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{
					IsError:      false,
					IsParseError: false,
					Query:        elastic.NewSearchSource(),
					Result:       []*aggregation.AggregationResponse{},
				},
				listQuery:      queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper:         fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с возвращением ошибки при парсинге запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: true,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{
					IsError:      false,
					IsParseError: true,
					Query:        elastic.NewSearchSource(),
					Result:       []*aggregation.AggregationResponse{},
				},
				listQuery:      queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper:         fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{
					IsError: false,
					Query:   elastic.NewSearchSource(),
					Result:  []*aggregation.AggregationResponse{},
				},
				listQuery:      queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper:         fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    []*aggregation.AggregationResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.GetAggregationDataByRequest(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAggregationDataByRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAggregationDataByRequest() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения списка сущностей по переданным ID
func Test_elasticSearchGraphQlRepository_GetByIds(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		ids        []string
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []RowData
		wantErr bool
	}{
		{
			name: "Тестирование с возвращением ошибки исполнителем запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: true,
					GetByIdResult:  &elastic.GetResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					SearchResult: []map[string]interface{}{},
				},
			},
			args: args{
				ids:        []string{"1", "1"},
				operations: nil,
			},
			want:    []map[string]interface{}{},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: false,
					GetByIdResult:  &elastic.GetResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				ids:        []string{"1", "1"},
				operations: nil,
			},
			want: []map[string]interface{}{
				{
					"id": 1,
				},
				{
					"id": 1,
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование без ошибок с пустым набором ID",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: false,
					GetByIdResult:  &elastic.GetResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				ids:        []string{},
				operations: nil,
			},
			want:    []map[string]interface{}{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.GetByIds(tt.args.ids, tt.args.operations)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByIds() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByIds() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения листинга сущностей
func Test_elasticSearchGraphQlRepository_GetListByGraphQlParams(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []RowData
		wantErr bool
	}{
		{
			name: "Тестирование возвращения ошибки, которая возникает при генерации запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery: queries.ListQueryBuilderMock{
					IsBuildError: true,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult: []map[string]interface{}{
						{"id": 1},
					},
				},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование возвращения ошибки, которая возникает при выполнении запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: true,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery: queries.ListQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult: []map[string]interface{}{
						{"id": 1},
					},
				},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование корректной обработки полученных данных",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery: queries.ListQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult: []map[string]interface{}{
						{"id": 1},
					},
				},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{},
			want: []map[string]interface{}{
				{"id": 1},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.GetListByGraphQlParams(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListByGraphQlParams() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListByGraphQlParams() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения листинга ID сущностей
func Test_elasticSearchGraphQlRepository_GetEntityIdsByFilterParameters(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		filter     whereOrHavingParser.Operation
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "Тестирование ошибки при генерации запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: true,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование ошибки при выполнении запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: true,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование корректного запроса",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    []string{"1"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.GetEntityIdsByFilterParameters(tt.args.filter, tt.args.operations)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetEntityIdsByFilterParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetEntityIdsByFilterParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование удаления сущностей по ID
func Test_elasticSearchGraphQlRepository_RemoveByIds(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		ids        []string
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Тестирование удаления сущностей, когда возвращается ошибка удаления",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsDeleteByIdError: true,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper:           fieldMapper.FieldMapperMock{},
			},
			args: args{
				ids:        []string{"1", "2"},
				operations: nil,
			},
			wantErr: true,
		},
		{
			name: "Тестирование удаления сущностей без ошибки",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsDeleteByIdError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper:           fieldMapper.FieldMapperMock{},
			},
			args: args{
				ids:        []string{"1", "2"},
				operations: nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование удаления сущностей без ошибки без ID",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsDeleteByIdError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper:           fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			if err := e.RemoveByIds(tt.args.ids, tt.args.operations); (err != nil) != tt.wantErr {
				t.Errorf("RemoveByIds() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование вставки объектов в индекс
func Test_elasticSearchGraphQlRepository_InsertObjects(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		objects    []map[string]interface{}
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantIds []string
		wantErr bool
	}{
		{
			name: "Тестирование вставки сущностей с ошибкой вставки",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsInsertError: true,
					InsertId:      "1",
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
				},
			},
			args: args{
				objects: []map[string]interface{}{
					{"test": "test"},
				},
				operations: nil,
			},
			wantIds: nil,
			wantErr: true,
		},
		{
			name: "Тестирование вставки сущностей с ошибкой маппинга",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsInsertError: false,
					InsertId:      "1",
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: true,
				},
			},
			args: args{
				objects: []map[string]interface{}{
					{"test": "test"},
				},
				operations: nil,
			},
			wantIds: nil,
			wantErr: true,
		},
		{
			name: "Тестирование вставки сущностей без ошибок",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsInsertError: false,
					InsertId:      "1",
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
				},
			},
			args: args{
				objects: []map[string]interface{}{
					{"test": "test"},
				},
				operations: nil,
			},
			wantIds: []string{"1"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			gotIds, err := e.InsertObjects(tt.args.objects, tt.args.operations)
			if (err != nil) != tt.wantErr {
				t.Errorf("InsertObjects() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotIds, tt.wantIds) {
				t.Errorf("InsertObjects() gotIds = %v, want %v", gotIds, tt.wantIds)
			}
		})
	}
}

// Тестирование вставки сущностей по переданным параметрам GraphQL
func Test_elasticSearchGraphQlRepository_InsertObjectsByGraphQlParameters(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *InsertOrUpdateResponse
		wantErr bool
	}{
		{
			name: "Тестирование без передачи объектов вставки",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
					IsInsertError: false,
					InsertId:      "1",
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery: queries.ListQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult: []map[string]interface{}{
						{"id": 1},
					},
				},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args:    args{},
			want:    &InsertOrUpdateResponse{},
			wantErr: false,
		},
		{
			name: "Тестирование без передачи объектов вставки",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError: false,
					SearchResult:  &elastic.SearchResult{},
					IsInsertError: false,
					InsertId:      "1",
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery: queries.ListQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult: []map[string]interface{}{
						{"id": 1},
					},
				},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{},
				log:            LoggerStubForTest{},
				logIncService:  &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: []map[string]interface{}{
							{"test": "test"},
							{"test-2": "test-2"},
						},
					},
				},
			},
			want: &InsertOrUpdateResponse{
				AffectedRows: 2,
				Returning: []map[string]interface{}{
					{"id": 1},
					{"id": 1},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.InsertObjectsByGraphQlParameters(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("InsertObjectsByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InsertObjectsByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование удаления сущностей по переданным параметрам GraphQL
func Test_elasticSearchGraphQlRepository_RemoveByGraphQlParams(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *RemoveResponse
		wantErr bool
	}{
		{
			name: "Тестирование удаления сущностей с ошибкой получения ID сущностей",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError:     false,
					SearchResult:      &elastic.SearchResult{},
					IsDeleteByIdError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: true,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование удаления сущностей с ошибкой получения ID сущностей",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError:     true,
					SearchResult:      &elastic.SearchResult{},
					IsDeleteByIdError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование удаления сущностей с ошибкой удаления сущностей по ID",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError:     false,
					SearchResult:      &elastic.SearchResult{},
					IsDeleteByIdError: true,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование удаления сущностей без ошибок",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsSearchError:     false,
					SearchResult:      &elastic.SearchResult{},
					IsDeleteByIdError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  elastic.NewSearchSource(),
					ParseResult:  []string{"1"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper:        fieldMapper.FieldMapperMock{},
			},
			args: args{},
			want: &RemoveResponse{
				AffectedRows:     1,
				RemovedEntityIds: []string{"1"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.RemoveByGraphQlParams(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("RemoveByGraphQlParams() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveByGraphQlParams() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование обновления сущностей по переданным ID и объекту
func Test_elasticSearchGraphQlRepository_UpdateEntitiesById(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		set        map[string]interface{}
		ids        []string
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Тестирование на пустом наборе ID",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsUpdateError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper:           fieldMapper.FieldMapperMock{},
			},
			args: args{
				set: map[string]interface{}{
					"test": 1,
				},
				ids:        []string{},
				operations: nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование ошибки при парсинге сущности обновления",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsUpdateError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: true,
				},
			},
			args: args{
				set: map[string]interface{}{
					"test": 1,
				},
				ids:        []string{"1"},
				operations: nil,
			},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки при обновлении сущности",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsUpdateError: true,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
				},
			},
			args: args{
				set: map[string]interface{}{
					"test": 1,
				},
				ids:        []string{"1"},
				operations: nil,
			},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsUpdateError: false,
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery:   queries.EntityIdsQueryBuilderMock{},
				log:              LoggerStubForTest{},
				logIncService:    &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
				},
			},
			args: args{
				set: map[string]interface{}{
					"test": 1,
				},
				ids:        []string{"1", "2"},
				operations: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			if err := e.UpdateEntitiesById(tt.args.set, tt.args.ids, tt.args.operations); (err != nil) != tt.wantErr {
				t.Errorf("UpdateEntitiesById() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование обновления сущностей по параметрам GraphQL запроса
func Test_elasticSearchGraphQlRepository_UpdateObjectsByGraphQlParameters(t *testing.T) {
	type fields struct {
		index            string
		executor         executor.QueryExecutorInterface
		aggregationQuery queries.AggregationQueryBuilderInterfaces
		listQuery        queries.ListQueryBuilderInterface
		entityIdsQuery   queries.EntityIdsQueryBuilderInterface
		log              configurable_logger.LoggerInterface
		logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
		mapper           fieldMapper.FieldMapperInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *InsertOrUpdateResponse
		wantErr bool
	}{
		{
			name: "Тестирование без ошибок",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: false,
					GetByIdResult:  &elastic.GetResult{},
					IsUpdateError:  false,
					IsSearchError:  false,
					SearchResult:   &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  &elastic.SearchSource{},
					ParseResult:  []string{"1", "2"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: RowData{
							"test": 1,
						},
					},
				},
			},
			want: &InsertOrUpdateResponse{
				AffectedRows: 2,
				Returning: []RowData{
					{"id": 1},
					{"id": 1},
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование с ошибкой получения ID сущностей",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: false,
					GetByIdResult:  &elastic.GetResult{},
					IsUpdateError:  false,
					IsSearchError:  false,
					SearchResult:   &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: true,
					BuildResult:  &elastic.SearchSource{},
					ParseResult:  []string{"1", "2"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: RowData{
							"test": 1,
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с ошибкой обновления сущностей",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: false,
					GetByIdResult:  &elastic.GetResult{},
					IsUpdateError:  true,
					IsSearchError:  false,
					SearchResult:   &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  &elastic.SearchSource{},
					ParseResult:  []string{"1", "2"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: RowData{
							"test": 1,
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование с ошибкой получения обновленных сущностей",
			fields: fields{
				index: "test",
				executor: executor.QueryExecutorMock{
					IsGetByIdError: true,
					GetByIdResult:  &elastic.GetResult{},
					IsUpdateError:  false,
					IsSearchError:  false,
					SearchResult:   &elastic.SearchResult{},
				},
				aggregationQuery: queries.AggregationQueryBuilderMock{},
				listQuery:        queries.ListQueryBuilderMock{},
				entityIdsQuery: queries.EntityIdsQueryBuilderMock{
					IsBuildError: false,
					BuildResult:  &elastic.SearchSource{},
					ParseResult:  []string{"1", "2"},
				},
				log:           LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				mapper: fieldMapper.FieldMapperMock{
					IsMapEntityToInsertError: false,
					GetResult: map[string]interface{}{
						"id": 1,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: RowData{
							"test": 1,
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := elasticSearchGraphQlRepository{
				index:            tt.fields.index,
				executor:         tt.fields.executor,
				aggregationQuery: tt.fields.aggregationQuery,
				listQuery:        tt.fields.listQuery,
				entityIdsQuery:   tt.fields.entityIdsQuery,
				log:              tt.fields.log,
				logIncService:    tt.fields.logIncService,
				mapper:           tt.fields.mapper,
			}
			got, err := e.UpdateObjectsByGraphQlParameters(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateObjectsByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateObjectsByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}
