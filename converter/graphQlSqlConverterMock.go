package converter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type graphQlSqlConverterMock struct {
	Result  interface{}
	IsError bool
}

// Конвертация в базовый тип, например в строку или число
func (g graphQlSqlConverterMock) ToBaseType(*graphql.Object, string, interface{}) (interface{}, error) {
	if g.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return g.Result, nil
}

// Конвертация в SQL like значение
func (g graphQlSqlConverterMock) ToSQLValue(*graphql.Object, string, interface{}) (string, error) {
	return "", nil
}
