package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Заглушка для тестирования
type processorMock struct {
	IsAvailable bool
	IsError     bool
	Result      elastic.Query
}

// Проверка доступности процессора
func (p processorMock) isAvailable(whereOrHavingParser.Operation) bool {
	return p.IsAvailable
}

// Генерация запроса
func (p processorMock) generate(whereOrHavingParser.Operation) (elastic.Query, error) {
	if p.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return p.Result, nil
}
