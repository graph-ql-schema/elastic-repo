package fieldMapper

import (
	"encoding/json"
	"fmt"
	"sort"
	"sync"

	"bitbucket.org/graph-ql-schema/elastic-repo/converter"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

var systemFields = []string{
	"_id",
	"_uid",
	"_score",
	"_type",
	"_index",
	"_shard",
	"_node",
}

// Единичный результат парсинга
type parsedItem = map[string]interface{}

// Результат асинхронного парсинга
type parseResult struct {
	Index  int
	Result map[string]interface{}
}

// Преобразует переданное название поля в реальное значение, если такое есть.
type fieldMapper struct {
	fieldsMap      map[string]string
	valueConverter converter.ConverterInterface
}

// Парсинг результатов получения сущности. Подменяет поля на имеющиеся в маппинге и возвращает результат
func (f fieldMapper) ParseGetRequest(result *elastic.GetResult) map[string]interface{} {
	if nil == result {
		return nil
	}

	var sourceItem parsedItem
	_ = json.Unmarshal(result.Source, &sourceItem)

	customValues := parsedItem{}
	customValues["_id"] = result.Id
	customValues["_uid"] = result.Uid
	customValues["_type"] = result.Type
	customValues["_index"] = result.Index

	resultItem := parsedItem{}
	for origin, val := range sourceItem {
		code := f.unmap(origin)
		val, err := f.valueConverter.Convert(code, val)
		if nil != err {
			val = nil
		}

		resultItem[code] = val
	}

	for dst, source := range f.fieldsMap {
		if val, ok := customValues[source]; ok {
			resultItem[dst] = val
		}
	}

	return resultItem
}

// Маппит сущность для вставки или обновления. Если в сущности присутствует ID,
// то возвращает его. Если присутствуют системные поля, то удаляет их.
func (f fieldMapper) MapEntityToInsertOrUpdate(
	entity map[string]interface{},
) (mappedEntity map[string]interface{}, id string, err error) {
	mappedEntity = map[string]interface{}{}
	for originCode, val := range entity {
		mappedCode := f.Map(originCode)
		switch true {
		// Если это ID, то выносим его отдельно
		case mappedCode == "_id":
			if id = fmt.Sprintf(`%v`, val); 0 == len(id) {
				return nil, "", fmt.Errorf(`invalid ID value`)
			}
			break
		// Если поле системное - скипаем
		case f.isSystemField(mappedCode):
			break
		// Остальные поля добавляем в результат
		default:
			mappedEntity[mappedCode] = val
		}
	}

	return mappedEntity, id, nil
}

// Проверяет, является ли переданное поле системным
func (f fieldMapper) isSystemField(field string) bool {
	for _, systemField := range systemFields {
		if systemField == field {
			return true
		}
	}

	return false
}

// Парсинг результатов поиска. Подменяет поля на имеющиеся в маппинге и возвращает результат
func (f fieldMapper) ParseSearchRequest(result *elastic.SearchResult) []map[string]interface{} {
	var wg sync.WaitGroup
	itemsChan := make(chan parseResult)
	resultChan := make(chan []parsedItem)
	completeChan := make(chan bool)

	defer close(itemsChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов
	go f.readResults(itemsChan, resultChan, completeChan)

	// Запускаем распараллеленную обработку результатов
	wg.Add(len(result.Hits.Hits))
	for index, item := range result.Hits.Hits {
		go f.parseSingleItem(&wg, item, index, itemsChan)
	}

	wg.Wait()
	completeChan <- true

	return <-resultChan
}

// Асинхронное чтение результатов парсинга
func (f fieldMapper) readResults(itemsChan chan parseResult, resultChan chan []parsedItem, completeChan chan bool) {
	results := []parseResult{}
	for {
		select {
		case item := <-itemsChan:
			results = append(results, item)
			break
		case _ = <-completeChan:
			sort.SliceStable(results, func(i, j int) bool {
				return results[i].Index < results[j].Index
			})

			items := []parsedItem{}
			for _, item := range results {
				items = append(items, item.Result)
			}

			resultChan <- items
			return
		}
	}
}

// Асинхронный парсинг элемента результата поиска
func (f fieldMapper) parseSingleItem(wg *sync.WaitGroup, item *elastic.SearchHit, index int, resultChan chan parseResult) {
	defer wg.Done()

	var sourceItem parsedItem
	_ = json.Unmarshal(item.Source, &sourceItem)

	customValues := parsedItem{}
	customValues["_id"] = item.Id
	customValues["_uid"] = item.Uid
	customValues["_type"] = item.Type
	customValues["_index"] = item.Index

	resultItem := parsedItem{}
	for origin, val := range sourceItem {
		code := f.unmap(origin)
		val, err := f.valueConverter.Convert(code, val)
		if nil != err {
			val = nil
		}

		resultItem[code] = val
	}

	for dst, source := range f.fieldsMap {
		if val, ok := customValues[source]; ok {
			resultItem[dst] = val
		}
	}

	resultChan <- parseResult{
		Index:  index,
		Result: resultItem,
	}
}

// Преобразование системного кода поля в картированное
func (f fieldMapper) unmap(field string) string {
	for dst, origin := range f.fieldsMap {
		if origin == field {
			return dst
		}
	}

	return field
}

// Получение корректного названия поля
func (f fieldMapper) Map(field string) string {
	if fieldName, ok := f.fieldsMap[field]; ok {
		return fieldName
	}

	return field
}

// Фабрика маппера
func NewFieldMapper(object *graphql.Object, fieldsMap map[string]string) FieldMapperInterface {
	return &fieldMapper{
		fieldsMap:      fieldsMap,
		valueConverter: converter.NewConverter(object),
	}
}
