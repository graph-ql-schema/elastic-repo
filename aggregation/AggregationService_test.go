package aggregation

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/requestedFieldsParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование генерации запроса
func TestAggregationService_GenerateQueryByGraphQlParameters(t *testing.T) {
	type fields struct {
		processors []aggregationProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []GenerationResult
	}{
		{
			name: "Тестирование генерации запроса при не доступных процессорах",
			fields: fields{
				processors: []aggregationProcessorInterface{
					&aggregationProcessorMock{
						IsAvailable:           false,
						QueryResults:          nil,
						ResultProcessCallback: nil,
					},
					&aggregationProcessorMock{
						IsAvailable:           false,
						QueryResults:          nil,
						ResultProcessCallback: nil,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{},
		},
		{
			name: "Тестирование генерации запроса при одном доступном и одном не доступном процессоре",
			fields: fields{
				processors: []aggregationProcessorInterface{
					&aggregationProcessorMock{
						IsAvailable:           false,
						QueryResults:          nil,
						ResultProcessCallback: nil,
					},
					&aggregationProcessorMock{
						IsAvailable: true,
						QueryResults: []GenerationResult{
							{
								Name:        "test_agg",
								Aggregation: elastic.NewTermsAggregation().Field("qtty"),
							},
						},
						ResultProcessCallback: nil,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "test_agg",
					Aggregation: elastic.NewTermsAggregation().Field("qtty"),
				},
			},
		},
		{
			name: "Тестирование генерации запроса при двух доступных процессорах",
			fields: fields{
				processors: []aggregationProcessorInterface{
					&aggregationProcessorMock{
						IsAvailable: true,
						QueryResults: []GenerationResult{
							{
								Name:        "test_query",
								Aggregation: elastic.NewTermsAggregation().Field("qtty"),
							},
						},
						ResultProcessCallback: nil,
					},
					&aggregationProcessorMock{
						IsAvailable: true,
						QueryResults: []GenerationResult{
							{
								Name:        "test_agg",
								Aggregation: elastic.NewTermsAggregation().Field("qtty"),
							},
						},
						ResultProcessCallback: nil,
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: []requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField(
							constants.MaxOperationSchemaKey,
							[]requestedFieldsParser.GraphQlRequestedFieldInterface{
								requestedFieldsParser.NewGraphQlRequestedField("qtty", nil),
							},
						),
					},
				},
			},
			want: []GenerationResult{
				{
					Name:        "test_query",
					Aggregation: elastic.NewTermsAggregation().Field("qtty"),
				},
				{
					Name:        "test_agg",
					Aggregation: elastic.NewTermsAggregation().Field("qtty"),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aggregationService{
				processors: tt.fields.processors,
			}
			if got := a.GenerateQueryByGraphQlParameters(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateQueryByGraphQlParameters() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга запроса
func Test_aggregationService_ParseResult(t *testing.T) {
	type fields struct {
		processors []aggregationProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		data   *elastic.AggregationBucketKeyItem
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *AggregationResponse
	}{
		{
			name: "Парсинг запроса несколькими процессорами",
			fields: fields{
				processors: []aggregationProcessorInterface{
					&aggregationProcessorMock{
						IsAvailable:  true,
						QueryResults: nil,
						ResultProcessCallback: func(result *AggregationResponse) {
							result.Count = 10
						},
					},
					&aggregationProcessorMock{
						IsAvailable:  true,
						QueryResults: nil,
						ResultProcessCallback: func(result *AggregationResponse) {
							result.Max = map[string]float64{
								"price": 15,
							}
						},
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{},
				data:   nil,
			},
			want: &AggregationResponse{
				Count: 10,
				Avg:   nil,
				Sum:   nil,
				Min:   nil,
				Max: map[string]float64{
					"price": 15,
				},
				Variants: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aggregationService{
				processors: tt.fields.processors,
			}
			if got := a.ParseResult(tt.args.params, tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseResult() = %v, want %v", got, tt.want)
			}
		})
	}
}
