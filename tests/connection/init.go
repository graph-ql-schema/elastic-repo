package connection

import (
	"context"
	"log"

	client2 "bitbucket.org/graph-ql-schema/elastic-repo/client"
	"bitbucket.org/graph-ql-schema/elastic-repo/tests/logger"
)

var Client client2.ElasticClientServiceInterface

func init() {
	servers := []string{
		"http://localhost:9200",
	}

	Client = client2.NewElasticClientService(
		servers,
		`elastica`,
		`elastica`,
		context.Background(),
		logger.LoggerFactory,
	)

	go func() {
		log.Fatal(Client.RunServerCheck(nil))
	}()

	client := Client.GetClient()
	exists, err := client.IndexExists("test_data").Do(context.Background())
	if err != nil {
		log.Fatal(err.Error())
	}
	if !exists {
		createIndex, err := client.CreateIndex("test_data").
			BodyString(`
			{
				"settings":{
					"number_of_shards": 256,
					"number_of_replicas": 1,
					"index.max_ngram_diff": 10,
					"analysis": {
						"analyzer": {
							"partial": {
								"type": "custom",
								"tokenizer": "standard",
								"filter": [
									"lowercase",
									"partial_filter"
								]
							}
						},
						"filter": {
							"partial_filter": {
								"type": "nGram",
								"min_gram": 1,
								"max_gram": 10
							}
						}
					}
				},
				"mappings":{
					"properties":{
						"name":{
							"type": "keyword",
							"fields":{
								"partial":{
									"search_analyzer":"partial",
									"analyzer":"partial",
									"type":"text"
								}
							}
						},
						"operations":{
							"type": "keyword",
							"fields":{
								"partial":{
									"search_analyzer":"partial",
									"analyzer":"partial",
									"type":"text"
								}
							}
						},
						"created":{
							"type":"date"
						},
						"num": {
							"type": "integer"
						},
						"flt": {
							"type": "float"
						},
						"active": {
							"type": "boolean"
						}
					}
				}
			}
		`).Do(Client.GetContext())
		if err != nil {
			log.Fatal(err.Error())
		}

		if !createIndex.Acknowledged {
			log.Fatal("Can't create primary index")
		}
	}
}
