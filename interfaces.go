package elastic_repo

import (
	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/sveshnikovwork/configurable-logger"
)

// Сырые данные
type RowData = map[string]interface{}

// Тип, описывающий результат обработки GraphQL запроса на вставку/обновление данных
type InsertOrUpdateResponse struct {
	AffectedRows int       `json:"affected_rows"`
	Returning    []RowData `json:"returning"`
}

// Результат удаления сущностей
type RemoveResponse struct {
	AffectedRows     int      `json:"affected_rows"`
	RemovedEntityIds []string `json:"ids"`
}

// Интерфейс репозитория ElasticSearch
type ElasticSearchGraphQlRepositoryInterface interface {
	// Получение данных агрегации для переданных параметров запроса
	GetAggregationDataByRequest(params sbuilder.Parameters) ([]*aggregation.AggregationResponse, error)

	// Получение сущностей по ID.
	GetByIds(ids []string, operations []configurable_logger.LogOperation) ([]RowData, error)

	// Получение листинга сущностей по переданным параметрам GraphQL
	GetListByGraphQlParams(params sbuilder.Parameters) ([]RowData, error)

	// Получение ID сущностей по переданным параметрам фильтрации
	GetEntityIdsByFilterParameters(
		filter whereOrHavingParser.Operation,
		operations []configurable_logger.LogOperation,
	) ([]string, error)

	// Удаление сущностей по переданным ID
	RemoveByIds(ids []string, operations []configurable_logger.LogOperation) error

	// Удаление сущностей по переданным параметрам GraphQL
	RemoveByGraphQlParams(params sbuilder.Parameters) (*RemoveResponse, error)

	// Вставка сущностей в индекс
	InsertObjects(objects []map[string]interface{}, operations []configurable_logger.LogOperation) (ids []string, err error)

	// Вставка сущностей в индекс по параметрам запроса GraphQL
	InsertObjectsByGraphQlParameters(params sbuilder.Parameters) (*InsertOrUpdateResponse, error)

	// Обновление сущностей в индексе по параметрам запроса GraphQL
	UpdateObjectsByGraphQlParameters(params sbuilder.Parameters) (*InsertOrUpdateResponse, error)

	// Обновление сущностей в индексе по переданному объекту и ID сущностей
	UpdateEntitiesById(set map[string]interface{}, ids []string, operations []configurable_logger.LogOperation) error
}
