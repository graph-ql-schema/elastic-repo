package queries

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/filter"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	configurable_logger "bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Тестирование парсинга ответа на запрос
func Test_entityIdsQueryBuilder_Parse(t *testing.T) {
	type fields struct {
		logger        configurable_logger.LoggerInterface
		logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
		queryBuilder  filter.QueryGeneratorInterface
	}
	type args struct {
		response   *elastic.SearchResult
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []string
	}{
		{
			name: "Тестирование парсинга ответа на запрос",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder:  filter.QueryGeneratorMock{},
			},
			args: args{
				response: &elastic.SearchResult{
					Hits: &elastic.SearchHits{
						Hits: []*elastic.SearchHit{
							{Id: "1"},
							{Id: "2"},
						},
					},
				},
				operations: nil,
			},
			want: []string{"1", "2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := entityIdsQueryBuilder{
				logger:        tt.fields.logger,
				logIncService: tt.fields.logIncService,
				queryBuilder:  tt.fields.queryBuilder,
			}
			if got := e.Parse(tt.args.response, tt.args.operations); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса
func Test_entityIdsQueryBuilder_Build(t *testing.T) {
	type fields struct {
		logger        configurable_logger.LoggerInterface
		logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
		queryBuilder  filter.QueryGeneratorInterface
	}
	type args struct {
		filter     whereOrHavingParser.Operation
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *elastic.SearchSource
		wantErr bool
	}{
		{
			name: "Тестирование ошибки при генерации параметров фильтрации",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "1"),
					IsError: true,
				},
			},
			args: args{
				filter:     whereOrHavingParser.NewSimpleOperation(constants.EqualsSchemaKey, "1", "test", "="),
				operations: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации запроса с пустым фильтром",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "1"),
					IsError: false,
				},
			},
			args: args{
				filter:     nil,
				operations: nil,
			},
			want:    elastic.NewSearchSource(),
			wantErr: false,
		},
		{
			name: "Тестирование генерации запроса",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "1"),
					IsError: false,
				},
			},
			args: args{
				filter:     whereOrHavingParser.NewSimpleOperation(constants.EqualsSchemaKey, "1", "test", "="),
				operations: nil,
			},
			want:    elastic.NewSearchSource().Query(elastic.NewTermQuery("test", "1")),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := entityIdsQueryBuilder{
				logger:        tt.fields.logger,
				logIncService: tt.fields.logIncService,
				queryBuilder:  tt.fields.queryBuilder,
			}
			got, err := e.Build(tt.args.filter, tt.args.operations)
			if (err != nil) != tt.wantErr {
				t.Errorf("Build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Build() got = %v, want %v", got, tt.want)
			}
		})
	}
}
