package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Генератор запросов OR
type orQueryGeneratorProcessor struct {
	multiProcessor multiProcessorInterface
}

// Проверка доступности процессора
func (o orQueryGeneratorProcessor) isAvailable(params whereOrHavingParser.Operation) bool {
	return params.Type() == constants.OrSchemaKey
}

// Генерация запроса
func (o orQueryGeneratorProcessor) generate(params whereOrHavingParser.Operation) (elastic.Query, error) {
	fields, err := o.multiProcessor.process(params.Value().([]whereOrHavingParser.Operation))
	if nil != err {
		return nil, fmt.Errorf(`OR operation: %v`, err.Error())
	}

	return elastic.NewBoolQuery().Should(fields...), nil
}

// Фабрика процессора
func newOrQueryGeneratorProcessor(multiProcessor multiProcessorInterface) queryGeneratorProcessorInterface {
	return &orQueryGeneratorProcessor{
		multiProcessor: multiProcessor,
	}
}
