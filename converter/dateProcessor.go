package converter

import (
	"reflect"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

// Процессор конвертации дат
type dateProcessor struct {
	object     *graphql.Object
	converter  gql_sql_converter.GraphQlSqlConverterInterface
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Проверка доступности процессора
func (d dateProcessor) isAvailable(field string) bool {
	fieldObj, ok := d.object.Fields()[field]
	if !ok {
		return false
	}

	return d.typeGetter.GetRootType(fieldObj.Type) == graphql.DateTime
}

// Конвертация значения
func (d dateProcessor) convert(field string, val interface{}) (interface{}, error) {
	valType := reflect.TypeOf(val).Kind()
	if valType == reflect.Float32 || valType == reflect.Float64 {
		val = reflect.ValueOf(val).Float() * 1000 * 1000
	}

	return d.converter.ToBaseType(d.object, field, val)
}

// Фабрика процессора
func newDateProcessor(object *graphql.Object) converterProcessorInterface {
	return &dateProcessor{
		object:     object,
		converter:  gql_sql_converter.NewGraphQlSqlConverter(),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
