package queries

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type EntityIdsQueryBuilderMock struct {
	IsBuildError bool
	BuildResult  *elastic.SearchSource
	ParseResult  []string
}

// Построение запроса листинга сущностей
func (e EntityIdsQueryBuilderMock) Build(whereOrHavingParser.Operation, []configurable_logger.LogOperation) (*elastic.SearchSource, error) {
	if e.IsBuildError {
		return nil, fmt.Errorf(`test`)
	}

	return e.BuildResult, nil
}

// Парсинг ответа на запрос
func (e EntityIdsQueryBuilderMock) Parse(*elastic.SearchResult, []configurable_logger.LogOperation) []string {
	return e.ParseResult
}
