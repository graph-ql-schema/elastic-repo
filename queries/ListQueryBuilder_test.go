package queries

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/elastic-repo/filter"
	"bitbucket.org/graph-ql-schema/elastic-repo/sort"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/orderParser"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/paginationParser"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Генерация запроса на листинг сущностей
func Test_listQueryBuilder_Build(t *testing.T) {
	type fields struct {
		logger        configurable_logger.LoggerInterface
		logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
		queryBuilder  filter.QueryGeneratorInterface
		sortBuilder   sort.SortGeneratorInterface
	}
	type args struct {
		params     sbuilder.Parameters
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *elastic.SearchSource
		wantErr bool
	}{
		{
			name: "Тестирование генерации запроса с параметрами Where",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "test"),
					IsError: false,
				},
				sortBuilder: sort.SortGeneratorMock{
					Result: nil,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
						Where: whereOrHavingParser.NewSimpleOperation(constants.EqualsSchemaKey, "test", "test", "="),
					},
				},
				operations: nil,
			},
			want: elastic.
				NewSearchSource().
				Query(elastic.NewTermQuery("test", "test")).
				From(0).
				Size(10),
			wantErr: false,
		},
		{
			name: "Тестирование генерации запроса с параметрами Where когда генератор возвращает ошибку",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "test"),
					IsError: true,
				},
				sortBuilder: sort.SortGeneratorMock{
					Result: nil,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
						Where: whereOrHavingParser.NewSimpleOperation(constants.EqualsSchemaKey, "test", "test", "="),
					},
				},
				operations: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации запроса без параметров Where",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "test"),
					IsError: false,
				},
				sortBuilder: sort.SortGeneratorMock{
					Result: nil,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
				},
				operations: nil,
			},
			want: elastic.
				NewSearchSource().
				From(0).
				Size(10),
			wantErr: false,
		},
		{
			name: "Тестирование генерации запроса без параметров Where",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "test"),
					IsError: false,
				},
				sortBuilder: sort.SortGeneratorMock{
					Result: nil,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
				},
				operations: nil,
			},
			want: elastic.
				NewSearchSource().
				From(0).
				Size(10),
			wantErr: false,
		},
		{
			name: "Тестирование генерации запроса с параметрами сортировки",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder: filter.QueryGeneratorMock{
					Result:  elastic.NewTermQuery("test", "test"),
					IsError: false,
				},
				sortBuilder: sort.SortGeneratorMock{
					Result: []elastic.Sorter{
						elastic.NewFieldSort("test").Asc(),
						elastic.NewFieldSort("test-2").Desc(),
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "test",
								Direction: "asc",
							},
							{
								Priority:  2,
								By:        "test-2",
								Direction: "desc",
							},
						},
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
				},
				operations: nil,
			},
			want: elastic.
				NewSearchSource().
				From(0).
				SortBy(
					elastic.NewFieldSort("test").Asc(),
					elastic.NewFieldSort("test-2").Desc(),
				).
				Size(10),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := listQueryBuilder{
				logger:        tt.fields.logger,
				logIncService: tt.fields.logIncService,
				queryBuilder:  tt.fields.queryBuilder,
				sortBuilder:   tt.fields.sortBuilder,
			}
			got, err := l.Build(tt.args.params, tt.args.operations)
			if (err != nil) != tt.wantErr {
				t.Errorf("Build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Build() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга результатов запроса
func Test_listQueryBuilder_Parse(t *testing.T) {
	type fields struct {
		logger        configurable_logger.LoggerInterface
		logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
		queryBuilder  filter.QueryGeneratorInterface
		sortBuilder   sort.SortGeneratorInterface
		mapper        fieldMapper.FieldMapperInterface
	}
	type args struct {
		response   *elastic.SearchResult
		operations []configurable_logger.LogOperation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []map[string]interface{}
	}{
		{
			name: "Тестирование парсинга результата запроса",
			fields: fields{
				logger:        LoggerStubForTest{},
				logIncService: &LoggerOperationIncreaseServiceMock{},
				queryBuilder:  filter.QueryGeneratorMock{},
				sortBuilder:   sort.SortGeneratorMock{},
				mapper: fieldMapper.FieldMapperMock{
					SearchResult: []map[string]interface{}{
						{
							"test": 1,
						},
					},
				},
			},
			args: args{
				response:   nil,
				operations: nil,
			},
			want: []map[string]interface{}{
				{
					"test": 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := listQueryBuilder{
				logger:        tt.fields.logger,
				logIncService: tt.fields.logIncService,
				queryBuilder:  tt.fields.queryBuilder,
				sortBuilder:   tt.fields.sortBuilder,
				mapper:        tt.fields.mapper,
			}
			got := l.Parse(tt.args.response, tt.args.operations)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
