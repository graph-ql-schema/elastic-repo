package converter

import (
	"reflect"
	"testing"

	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

// Тестирование доступности. Должно быть всегда true
func Test_simpleProcessor_isAvailable(t *testing.T) {
	type fields struct {
		object    *graphql.Object
		converter gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		in0 string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "Тестирование доступности",
			fields: fields{},
			args: args{
				in0: "test",
			},
			want: true,
		},
		{
			name:   "Тестирование доступности",
			fields: fields{},
			args: args{
				in0: "",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleProcessor{
				object:    tt.fields.object,
				converter: tt.fields.converter,
			}
			if got := s.isAvailable(tt.args.in0); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации
func Test_simpleProcessor_convert(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Boolean,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.String,
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		object    *graphql.Object
		converter gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		field string
		val   interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование конвертации",
			fields: fields{
				object: objectConfig,
				converter: graphQlSqlConverterMock{
					Result:  "test",
					IsError: false,
				},
			},
			args: args{
				field: "field_1",
				val:   "test",
			},
			want:    "test",
			wantErr: false,
		},
		{
			name: "Тестирование конвертации",
			fields: fields{
				object: objectConfig,
				converter: graphQlSqlConverterMock{
					Result:  "test",
					IsError: true,
				},
			},
			args: args{
				field: "field_1",
				val:   "test",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleProcessor{
				object:    tt.fields.object,
				converter: tt.fields.converter,
			}
			got, err := s.convert(tt.args.field, tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
