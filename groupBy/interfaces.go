package groupBy

import (
	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/olivere/elastic/v7"
)

// Сервис группировки для запросов агрегации
type GroupByServiceInterface interface {
	// Генерация запроса группировки
	GenerateQuery(params sbuilder.Parameters) (code string, aggregation elastic.Aggregation)

	// Парсинг результатов запроса агрегации
	ParseResponse(params sbuilder.Parameters, response *elastic.SearchResult) []*aggregation.AggregationResponse
}
