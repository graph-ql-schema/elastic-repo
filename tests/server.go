package main

import (
	"log"

	"bitbucket.org/graph-ql-schema/elastic-repo"
	"bitbucket.org/graph-ql-schema/elastic-repo/tests/connection"
	"bitbucket.org/graph-ql-schema/elastic-repo/tests/logger"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/types"
)

var loggerFactory = logger.LoggerFactory
var schemaBuilder = sbuilder.NewGraphQlSchemaBuilder(loggerFactory)
var repository = elastic_repo.NewElasticSearchGraphQlRepository(
	"test_data",
	connection.Client,
	testObject,
	fieldsMap,
	logger.LoggerFactory,
)

func main() {
	schemaBuilder.RegisterEntity(testObject, sbuilder.EntityQueries{
		sbuilder.ListQuery: func(params types.Parameters) (interface{}, error) {
			return repository.GetListByGraphQlParams(params)
		},
		sbuilder.InsertMutation: func(params types.Parameters) (interface{}, error) {
			return repository.InsertObjectsByGraphQlParameters(params)
		},
		sbuilder.AggregateQuery: func(params types.Parameters) (interface{}, error) {
			return repository.GetAggregationDataByRequest(params)
		},
		sbuilder.DeleteMutation: func(params types.Parameters) (interface{}, error) {
			return repository.RemoveByGraphQlParams(params)
		},
		sbuilder.UpdateMutation: func(params types.Parameters) (interface{}, error) {
			return repository.UpdateObjectsByGraphQlParameters(params)
		},
	}, nil)

	schemaBuilder.SetServerConfig(sbuilder.GraphQlServerConfig{
		Host: "0.0.0.0",
		Port: 9000,
		Uri:  "/graphql",
	})

	server, err := schemaBuilder.BuildServer()
	if nil != err {
		log.Fatal(err)
	}

	log.Fatal(server.Run())
}
