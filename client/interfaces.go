package client

import (
	"context"

	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Интерфейс клиента ElasticSearch
type ElasticClientServiceInterface interface {
	// Получение актуального клиента
	GetClient() *elastic.Client

	// Получение контекста запросов
	GetContext() context.Context

	// Остановка сервиса
	GracefulShutdown(operations []configurable_logger.LogOperation)

	// Запуск сервиса. Позволяет обновлять клиент всякий раз, когда один из серверов не доступен
	RunServerCheck(operations []configurable_logger.LogOperation) error
}
