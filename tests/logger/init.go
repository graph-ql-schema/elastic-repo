package logger

import "bitbucket.org/sveshnikovwork/configurable-logger"

var LoggerFactory = configurable_logger.LoggerFactoryGenerator(false)
