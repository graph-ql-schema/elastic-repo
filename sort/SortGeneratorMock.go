package sort

import (
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/orderParser"
	"github.com/olivere/elastic/v7"
)

// Подставка для тестирования
type SortGeneratorMock struct {
	Result []elastic.Sorter
}

// Генерация параметров сортировки
func (s SortGeneratorMock) Generate([]orderParser.Order) []elastic.Sorter {
	return s.Result
}
