package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Фабрика поискового запроса
type tSearchFactory = func(string, interface{}) elastic.Query

// Генератор запроса строгого сравнения
type searchQueryProcessor struct {
	fieldsMapper fieldMapper.FieldMapperInterface
	factory      tSearchFactory
	operator     string
}

// Проверка доступности процессора
func (e searchQueryProcessor) isAvailable(params whereOrHavingParser.Operation) bool {
	return params.Type() == e.operator
}

// Генерация запроса
func (e searchQueryProcessor) generate(params whereOrHavingParser.Operation) (elastic.Query, error) {
	return e.factory(e.fieldsMapper.Map(params.Field()), params.Value()), nil
}

// Фабрика процессора Equals
func newEqualsQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			return elastic.NewTermQuery(s, i)
		},
		operator: constants.EqualsSchemaKey,
	}
}

// Фабрика процессора Like
func newLikeQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			return elastic.NewMatchQuery(fmt.Sprintf(`%v.partial`, s), i)
		},
		operator: constants.LikeSchemaKey,
	}
}

// Фабрика процессора In
func newInQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			parsedValues := i.([]interface{})

			return elastic.NewTermsQuery(s, parsedValues...)
		},
		operator: constants.InSchemaKey,
	}
}

// Фабрика процессора More
func newMoreQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			return elastic.NewRangeQuery(s).Gt(i)
		},
		operator: constants.MoreSchemaKey,
	}
}

// Фабрика процессора More or Equals
func newMoreOrEqualsQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			return elastic.NewRangeQuery(s).Gte(i)
		},
		operator: constants.MoreOrEqualsSchemaKey,
	}
}

// Фабрика процессора Less
func newLessQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			return elastic.NewRangeQuery(s).Lt(i)
		},
		operator: constants.MoreSchemaKey,
	}
}

// Фабрика процессора Less or Equals
func newLessOrEqualsQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			return elastic.NewRangeQuery(s).Lte(i)
		},
		operator: constants.MoreOrEqualsSchemaKey,
	}
}

// Фабрика процессора Between
func newBetweenQueryProcessor(fieldsMapper fieldMapper.FieldMapperInterface) queryGeneratorProcessorInterface {
	return &searchQueryProcessor{
		fieldsMapper: fieldsMapper,
		factory: func(s string, i interface{}) elastic.Query {
			parsedVal := i.(whereOrHavingParser.BetweenValue)

			return elastic.NewRangeQuery(s).Gte(parsedVal.From).Lte(parsedVal.To)
		},
		operator: constants.MoreOrEqualsSchemaKey,
	}
}
