package converter

import "fmt"

// Подставка для тестирования
type converterProcessorMock struct {
	IsAvailable bool
	IsError     bool
	Result      interface{}
}

// Проверка доступности процессора
func (c converterProcessorMock) isAvailable(string) bool {
	return c.IsAvailable
}

// Конвертация значения
func (c converterProcessorMock) convert(string, interface{}) (interface{}, error) {
	if c.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return c.Result, nil
}
