package elastic_repo

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/elastic-repo/client"
	"bitbucket.org/graph-ql-schema/elastic-repo/executor"
	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/elastic-repo/queries"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/graphql-go/graphql"
)

// Интерфейс репозитория ElasticSearch
type elasticSearchGraphQlRepository struct {
	index            string
	executor         executor.QueryExecutorInterface
	aggregationQuery queries.AggregationQueryBuilderInterfaces
	listQuery        queries.ListQueryBuilderInterface
	entityIdsQuery   queries.EntityIdsQueryBuilderInterface
	log              configurable_logger.LoggerInterface
	logIncService    configurable_logger.LoggerOperationIncreaseServiceInterface
	mapper           fieldMapper.FieldMapperInterface
}

// Обновление сущностей в индексе по переданному объекту и ID сущностей
func (e elasticSearchGraphQlRepository) UpdateEntitiesById(
	set map[string]interface{},
	ids []string,
	operations []configurable_logger.LogOperation,
) error {
	if 0 == len(ids) {
		return nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	var wg sync.WaitGroup
	ops := e.logIncService.IncreaseOperations(operations)

	errors := make(chan error)
	errResChan := make(chan error)
	completeChan := make(chan bool)

	defer close(errors)
	defer close(errResChan)
	defer close(completeChan)

	// Запускаем чтение результатов
	go e.readErrors(errors, errResChan, completeChan)

	// Запускаем асинхронное удаление сущностей
	wg.Add(len(ids))
	for _, id := range ids {
		go e.updateById(id, set, ops, &wg, errors)
	}

	wg.Wait()
	completeChan <- true

	err := <-errResChan

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	e.log.Notice(configurable_logger.LogParameters{
		Code: 201,
		Message: fmt.Sprintf(
			"Updated entities from index '%v'. Execution time: %v ms",
			e.index,
			finish-starts,
		),
		Data: map[string]interface{}{
			"ids":    ids,
			"errors": err,
			"set":    set,
		},
		Operations: ops,
	})

	return err
}

// Удаление сущности по переданному ID. Для асинхронной обработки.
func (e elasticSearchGraphQlRepository) updateById(
	id string,
	set map[string]interface{},
	operations []configurable_logger.LogOperation,
	wg *sync.WaitGroup,
	errChan chan error,
) {
	defer wg.Done()

	ops := e.logIncService.IncreaseOperations(operations)
	mappedEntity, _, err := e.mapper.MapEntityToInsertOrUpdate(set)
	if nil != err {
		e.log.Debug(configurable_logger.LogParameters{
			Code:    400,
			Message: fmt.Sprintf("Failed to parse set data: %v", err.Error()),
			Data: map[string]interface{}{
				"set": set,
				"err": err,
			},
			Operations: ops,
		})

		errChan <- err
		return
	}

	if 0 == len(mappedEntity) {
		return
	}

	err = e.executor.ExecuteUpdateQuery(e.index, id, mappedEntity, ops)
	if nil != err {
		errChan <- err
	}
}

// Обновление сущностей в индексе по параметрам запроса GraphQL
func (e elasticSearchGraphQlRepository) UpdateObjectsByGraphQlParameters(params sbuilder.Parameters) (*InsertOrUpdateResponse, error) {
	var ops []configurable_logger.LogOperation
	if nil != params.Context {
		ops = params.Context.Value(`operations`).([]configurable_logger.LogOperation)
	}

	ops = e.logIncService.IncreaseOperations(ops)
	ids, err := e.GetEntityIdsByFilterParameters(params.Arguments.Where, ops)
	if nil != err {
		return nil, err
	}

	err = e.UpdateEntitiesById(params.Arguments.Set, ids, ops)
	if nil != err {
		return nil, err
	}

	entities, err := e.GetByIds(ids, ops)
	if nil != err {
		return nil, err
	}

	return &InsertOrUpdateResponse{
		AffectedRows: len(entities),
		Returning:    entities,
	}, nil
}

// Удаление сущностей по переданным параметрам GraphQL
func (e elasticSearchGraphQlRepository) RemoveByGraphQlParams(params sbuilder.Parameters) (*RemoveResponse, error) {
	var ops []configurable_logger.LogOperation
	if nil != params.Context {
		ops = params.Context.Value(`operations`).([]configurable_logger.LogOperation)
	}

	ops = e.logIncService.IncreaseOperations(ops)
	ids, err := e.GetEntityIdsByFilterParameters(params.Arguments.Where, ops)
	if nil != err {
		return nil, err
	}

	err = e.RemoveByIds(ids, ops)
	if nil != err {
		return nil, err
	}

	return &RemoveResponse{
		AffectedRows:     len(ids),
		RemovedEntityIds: ids,
	}, nil
}

// Вставка сущностей в индекс по параметрам запроса GraphQL
func (e elasticSearchGraphQlRepository) InsertObjectsByGraphQlParameters(params sbuilder.Parameters) (*InsertOrUpdateResponse, error) {
	if 0 == len(params.Arguments.Objects) {
		return &InsertOrUpdateResponse{}, nil
	}

	var ops []configurable_logger.LogOperation
	if nil != params.Context {
		ops = params.Context.Value(`operations`).([]configurable_logger.LogOperation)
	}

	ops = e.logIncService.IncreaseOperations(ops)

	ids, err := e.InsertObjects(params.Arguments.Objects, ops)
	if nil != err {
		return nil, err
	}

	// Получаем вставленные значения при помощи запроса единичных сущностей.
	// Так можно нивелировать таймаут на обновление индекса в ElasticSearch
	entities, err := e.GetByIds(ids, ops)
	if nil != err {
		return nil, err
	}

	return &InsertOrUpdateResponse{
		AffectedRows: len(entities),
		Returning:    entities,
	}, nil
}

// Вставка сущностей в индекс
func (e elasticSearchGraphQlRepository) InsertObjects(
	objects []map[string]interface{},
	operations []configurable_logger.LogOperation,
) (ids []string, err error) {
	if 0 == len(objects) {
		return []string{}, nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)

	var wg sync.WaitGroup
	ops := e.logIncService.IncreaseOperations(operations)

	idsChan := make(chan string)
	errChan := make(chan error)
	resultChan := make(chan []string)
	errResultChan := make(chan error)
	completeChan := make(chan bool)

	defer close(idsChan)
	defer close(errChan)
	defer close(resultChan)
	defer close(errResultChan)
	defer close(completeChan)

	// Запускаем получение ID
	go e.readInsertIds(idsChan, errChan, resultChan, errResultChan, completeChan)

	// Запускаем асинхронное добавление сущностей
	wg.Add(len(objects))
	for num, object := range objects {
		go e.insertSingleItem(&wg, num, object, idsChan, errChan, ops)
	}

	wg.Wait()
	completeChan <- true

	ids = <-resultChan
	err = <-errResultChan

	if nil != err {
		e.log.Warning(configurable_logger.LogParameters{
			Code:    501,
			Message: fmt.Sprintf("Failed to insert entities to index '%v'. Error: %v", e.index, err.Error()),
			Data: map[string]interface{}{
				"entities": objects,
				"err":      err,
			},
			Operations: ops,
		})

		_ = e.RemoveByIds(ids, ops)

		return nil, err
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	e.log.Notice(configurable_logger.LogParameters{
		Code: 205,
		Message: fmt.Sprintf(
			"Created entities in index '%v'. Execution time: %v ms",
			e.index,
			finish-starts,
		),
		Data: map[string]interface{}{
			"entities": objects,
			"ids":      ids,
		},
		Operations: ops,
	})

	return ids, nil
}

// Чтение ID из потока асинхронной вставки
func (e elasticSearchGraphQlRepository) readInsertIds(
	idsChan chan string,
	errChan chan error,
	resultChan chan []string,
	errResultChan chan error,
	completeChan chan bool,
) {
	errors := []error{}
	ids := []string{}

	for {
		select {
		case id := <-idsChan:
			if 0 != len(id) {
				ids = append(ids, id)
			}
			break
		case err := <-errChan:
			if nil != err {
				errors = append(errors, err)
			}
			break
		case _ = <-completeChan:
			messages := []string{}
			for i, err := range errors {
				messages = append(messages, fmt.Sprintf(`%v) %v;`, i+1, err.Error()))
			}

			var err error
			if 0 != len(messages) {
				err = fmt.Errorf(`some errors occured: %v`, strings.Join(messages, ` `))
			}

			resultChan <- ids
			errResultChan <- err

			return
		}
	}
}

// Вставка элемента сущности в БД. После вставки значения возвращает ID новой сущности.
// Если в ходе работы возникает ошибка - возвращает ее. Для понятности возвращает еще и
// номер сущности при массовой обработке.
func (e elasticSearchGraphQlRepository) insertSingleItem(
	wg *sync.WaitGroup,
	num int,
	item map[string]interface{},
	idsChan chan string,
	errChan chan error,
	operations []configurable_logger.LogOperation,
) {
	defer wg.Done()

	ops := e.logIncService.IncreaseOperations(operations)

	mappedEntity, id, err := e.mapper.MapEntityToInsertOrUpdate(item)
	if nil != err {
		err = fmt.Errorf(`item %v: %v`, num, err.Error())
		e.log.Debug(configurable_logger.LogParameters{
			Code:    400,
			Message: fmt.Sprintf("Failed to parse item data: %v", err.Error()),
			Data: map[string]interface{}{
				"item": item,
				"err":  err,
			},
			Operations: ops,
		})

		errChan <- err
		return
	}

	id, err = e.executor.ExecuteInsertQuery(e.index, id, mappedEntity, ops)
	if nil != err {
		errChan <- fmt.Errorf(`item %v: %v`, num, err.Error())
		return
	}

	idsChan <- id
}

// Удаление сущностей по переданным ID
func (e elasticSearchGraphQlRepository) RemoveByIds(ids []string, operations []configurable_logger.LogOperation) error {
	if 0 == len(ids) {
		return nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	var wg sync.WaitGroup
	ops := e.logIncService.IncreaseOperations(operations)

	errors := make(chan error)
	errResChan := make(chan error)
	completeChan := make(chan bool)

	defer close(errors)
	defer close(errResChan)
	defer close(completeChan)

	// Запускаем чтение результатов
	go e.readErrors(errors, errResChan, completeChan)

	// Запускаем асинхронное удаление сущностей
	wg.Add(len(ids))
	for _, id := range ids {
		go e.removeById(id, ops, &wg, errors)
	}

	wg.Wait()
	completeChan <- true

	err := <-errResChan

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	e.log.Notice(configurable_logger.LogParameters{
		Code: 201,
		Message: fmt.Sprintf(
			"Removed entities from index '%v'. Execution time: %v ms",
			e.index,
			finish-starts,
		),
		Data: map[string]interface{}{
			"ids":    ids,
			"errors": err,
		},
		Operations: ops,
	})

	return err
}

// Чтение ошибок из общего пула сообщений об ошибках при множественной обработке
func (e elasticSearchGraphQlRepository) readErrors(
	errChan chan error,
	errResChan chan error,
	completeChan chan bool,
) {
	errors := []error{}
	for {
		select {
		case err := <-errChan:
			if nil != err {
				errors = append(errors, err)
			}
			break
		case _ = <-completeChan:
			messages := []string{}
			for i, err := range errors {
				messages = append(messages, fmt.Sprintf(`%v) %v;`, i+1, err.Error()))
			}

			var err error
			if 0 != len(messages) {
				err = fmt.Errorf(`some errors occured: %v`, strings.Join(messages, ` `))
			}

			errResChan <- err
			return
		}
	}
}

// Удаление сущности по переданному ID. Для асинхронной обработки.
func (e elasticSearchGraphQlRepository) removeById(
	id string,
	operations []configurable_logger.LogOperation,
	wg *sync.WaitGroup,
	errChan chan error,
) {
	defer wg.Done()

	ops := e.logIncService.IncreaseOperations(operations)
	err := e.executor.ExecuteRemoveByIdQuery(e.index, id, ops)

	if nil != err {
		errChan <- err
	}
}

// Получение ID сущностей по переданным параметрам фильтрации
func (e elasticSearchGraphQlRepository) GetEntityIdsByFilterParameters(
	filter whereOrHavingParser.Operation,
	operations []configurable_logger.LogOperation,
) ([]string, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	ops := e.logIncService.IncreaseOperations(operations)
	query, err := e.entityIdsQuery.Build(filter, ops)
	if nil != err {
		return nil, err
	}

	response, err := e.executor.ExecuteSearchQuery(e.index, query, ops)
	if nil != err {
		return nil, err
	}

	result := e.entityIdsQuery.Parse(response, ops)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	e.log.Info(configurable_logger.LogParameters{
		Code: 200,
		Message: fmt.Sprintf(
			"Got list of entities ids from index '%v'. Execution time: %v ms",
			e.index,
			finish-starts,
		),
		Data:       result,
		Operations: ops,
	})

	return result, nil
}

// Получение листинга сущностей по переданным параметрам GraphQL
func (e elasticSearchGraphQlRepository) GetListByGraphQlParams(params sbuilder.Parameters) ([]RowData, error) {
	if nil != params.Arguments.Pagination {
		if 0 == params.Arguments.Pagination.Limit {
			return []RowData{}, nil
		}
	}

	var ops []configurable_logger.LogOperation
	if nil != params.Context {
		ops = params.Context.Value(`operations`).([]configurable_logger.LogOperation)
	}

	ops = e.logIncService.IncreaseOperations(ops)
	query, err := e.listQuery.Build(params, ops)
	if nil != err {
		return nil, err
	}

	response, err := e.executor.ExecuteSearchQuery(e.index, query, ops)
	if nil != err {
		return nil, err
	}

	result := e.listQuery.Parse(response, ops)
	e.log.Info(configurable_logger.LogParameters{
		Code:       200,
		Message:    fmt.Sprintf("Got list of entities from index '%v'", e.index),
		Data:       result,
		Operations: ops,
	})

	return result, nil
}

// Получение сущностей по ID.
func (e elasticSearchGraphQlRepository) GetByIds(
	ids []string,
	operations []configurable_logger.LogOperation,
) ([]RowData, error) {
	if 0 == len(ids) {
		return []RowData{}, nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	ops := e.logIncService.IncreaseOperations(operations)

	var wg sync.WaitGroup
	errChan := make(chan error)
	itemsChan := make(chan RowData)
	errResultChan := make(chan error)
	itemsResultChan := make(chan []RowData)
	completeChan := make(chan bool)

	defer close(errChan)
	defer close(itemsChan)
	defer close(errResultChan)
	defer close(itemsResultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов
	go e.parseRowResults(errChan, itemsChan, errResultChan, itemsResultChan, completeChan)

	wg.Add(len(ids))
	for _, id := range ids {
		go e.getById(&wg, errChan, itemsChan, id, ops)
	}

	wg.Wait()
	completeChan <- true

	err := <-errResultChan
	items := <-itemsResultChan

	finish := time.Now().UnixNano() / int64(time.Millisecond)

	e.log.Info(configurable_logger.LogParameters{
		Code: 201,
		Message: fmt.Sprintf(
			"Got entities from index '%v' with ID's (%v). Execution time: %v ms",
			e.index,
			strings.Join(ids, ", "),
			finish-starts,
		),
		Data: map[string]interface{}{
			"items": items,
			"error": err,
		},
		Operations: ops,
	})

	return items, err
}

// Асинхронное чтение из каналов получения сущностей по ID
func (e elasticSearchGraphQlRepository) parseRowResults(
	errChan chan error,
	itemsChan chan RowData,
	errResultChan chan error,
	itemsResultChan chan []RowData,
	completeChan chan bool,
) {
	items := []RowData{}
	errors := []error{}

	for {
		select {
		case item := <-itemsChan:
			if nil != item {
				items = append(items, item)
			}
			break
		case err := <-errChan:
			if nil != err {
				errors = append(errors, err)
			}
			break
		case _ = <-completeChan:
			messages := []string{}
			for i, err := range errors {
				messages = append(messages, fmt.Sprintf(`%v) %v;`, i+1, err.Error()))
			}

			var err error
			if 0 != len(messages) {
				err = fmt.Errorf(`some errors occured: %v`, strings.Join(messages, ` `))
			}

			if nil != err {
				items = []RowData{}
			}

			errResultChan <- err
			itemsResultChan <- items

			return
		}
	}
}

// Асинхронное получение сущности по ID
func (e elasticSearchGraphQlRepository) getById(
	wg *sync.WaitGroup,
	errChan chan error,
	itemsChan chan RowData,
	id string,
	operations []configurable_logger.LogOperation,
) {
	defer wg.Done()

	ops := e.logIncService.IncreaseOperations(operations)
	resp, err := e.executor.ExecuteGetByIdQuery(e.index, id, ops)

	if nil != err {
		errChan <- err
		return
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	result := e.mapper.ParseGetRequest(resp)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	e.log.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Parsed get result. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"result": result,
		},
		Operations: ops,
	})

	itemsChan <- result
}

// Получение данных агрегации для переданных параметров запроса
func (e elasticSearchGraphQlRepository) GetAggregationDataByRequest(params sbuilder.Parameters) ([]*aggregation.AggregationResponse, error) {
	if nil != params.Arguments.Pagination {
		if 0 == params.Arguments.Pagination.Limit {
			return []*aggregation.AggregationResponse{}, nil
		}
	}

	var ops []configurable_logger.LogOperation
	if nil != params.Context {
		ops = params.Context.Value(`operations`).([]configurable_logger.LogOperation)
	}

	ops = e.logIncService.IncreaseOperations(ops)
	query, err := e.aggregationQuery.Build(params, ops)
	if nil != err {
		return nil, err
	}

	result, err := e.executor.ExecuteSearchQuery(e.index, query, ops)
	if nil != err {
		return nil, err
	}

	data, err := e.aggregationQuery.Parse(params, result, ops)
	if nil != err {
		return nil, err
	}

	e.log.Info(configurable_logger.LogParameters{
		Code:    200,
		Message: fmt.Sprintf(`Got aggregation data from index '%v'`, e.index),
		Data: map[string]interface{}{
			"data": data,
		},
		Operations: ops,
	})

	return data, nil
}

// Фабрика репозитория
func NewElasticSearchGraphQlRepository(
	index string,
	client client.ElasticClientServiceInterface,
	object *graphql.Object,
	fieldsMap map[string]string,
	factory configurable_logger.TLoggerFactory,
) ElasticSearchGraphQlRepositoryInterface {
	return &elasticSearchGraphQlRepository{
		index:            index,
		executor:         executor.NewQueryExecutor(client, factory),
		aggregationQuery: queries.NewAggregationQueryBuilder(object, fieldsMap, factory),
		listQuery:        queries.NewListQueryBuilder(object, fieldsMap, factory),
		entityIdsQuery:   queries.NewEntityIdsQueryBuilder(object, fieldsMap, factory),
		log:              factory(`ElasticSearchGraphQlRepository`),
		logIncService:    configurable_logger.LoggerOperationIncreaseService(),
		mapper:           fieldMapper.NewFieldMapper(object, fieldsMap),
	}
}
