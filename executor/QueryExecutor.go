package executor

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/graph-ql-schema/elastic-repo/client"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Сервис исполнения запросов
type queryExecutor struct {
	log           configurable_logger.LoggerInterface
	logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
	client        client.ElasticClientServiceInterface
}

// Выполнение запроса обновления сущности в ElasticSearch
func (q queryExecutor) ExecuteUpdateQuery(
	index string,
	id string,
	data interface{},
	operations []configurable_logger.LogOperation,
) error {
	if 0 == len(id) {
		return nil
	}

	ops := q.logIncService.IncreaseOperations(operations)
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	_, err := q.client.
		GetClient().
		Update().
		Index(index).
		Doc(data).
		Id(id).
		Do(q.client.GetContext())

	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if err != nil {
		err = fmt.Errorf(`entity update error: %v`, err.Error())
		q.log.Error(configurable_logger.LogParameters{
			Code:    505,
			Message: fmt.Sprintf("Failed to update entity with ID '%v' in index '%v'. Error: %v", id, index, err.Error()),
			Data: map[string]interface{}{
				"set":   data,
				"id":    id,
				"index": index,
				"err":   err,
			},
			Operations: ops,
		})

		return err
	}

	q.log.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Updated entity in index '%v'. ID: %v. Execution time: %v ms", index, id, finish-starts),
		Data: map[string]interface{}{
			"set":   data,
			"id":    id,
			"index": index,
		},
		Operations: ops,
	})

	return nil
}

// Выполнение запроса вставки сущности в ElasticSearch
func (q queryExecutor) ExecuteInsertQuery(
	index string,
	id string,
	data interface{},
	operations []configurable_logger.LogOperation,
) (insertedId string, err error) {
	ops := q.logIncService.IncreaseOperations(operations)
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	query := q.client.GetClient().
		Index().
		Index(index)

	if 0 != len(id) {
		query = query.Id(id)
	}

	insertedObject, err := query.BodyJson(data).Do(q.client.GetContext())
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if err != nil {
		q.log.Error(configurable_logger.LogParameters{
			Code:    505,
			Message: fmt.Sprintf("Failed to insert entity to index '%v'. Error: %v", index, err.Error()),
			Data: map[string]interface{}{
				"entity": data,
				"id":     id,
				"err":    err,
			},
			Operations: ops,
		})

		return "", err
	}

	q.log.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Insert entity to index '%v'. ID: %v. Execution time: %v ms", index, insertedObject.Id, finish-starts),
		Data: map[string]interface{}{
			"entity": data,
			"id":     insertedObject.Id,
		},
		Operations: ops,
	})

	return insertedObject.Id, nil
}

// Выполнение запроса удаления сущности по ID в ElasticSearch
func (q queryExecutor) ExecuteRemoveByIdQuery(index string, id string, operations []configurable_logger.LogOperation) error {
	ops := q.logIncService.IncreaseOperations(operations)
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	_, err := q.client.GetClient().
		Delete().
		Index(index).
		Id(id).
		Do(q.client.GetContext())
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	q.log.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Exucuted ElasticSearch 'delete' query. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"context": q.client.GetContext(),
			"id":      id,
			"index":   index,
		},
		Operations: ops,
	})

	if nil == err {
		return nil
	}

	err = fmt.Errorf(`item %v: %v`, id, err.Error())
	q.log.Error(configurable_logger.LogParameters{
		Code:    500,
		Message: "Failed to execute ElasticSearch delete request",
		Data: map[string]interface{}{
			"err":     err,
			"context": q.client.GetContext(),
			"id":      id,
			"index":   index,
		},
		Operations: ops,
	})

	return err
}

// Выполнение запроса получения сущности по ID в ElasticSearch
func (q queryExecutor) ExecuteGetByIdQuery(
	index string,
	id string,
	operations []configurable_logger.LogOperation,
) (*elastic.GetResult, error) {
	ops := q.logIncService.IncreaseOperations(operations)

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	resp, err := q.client.GetClient().
		Get().
		Index(index).
		Id(id).
		Do(q.client.GetContext())
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	q.log.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Exucuted ElasticSearch 'get' query. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"context": q.client.GetContext(),
			"id":      id,
			"index":   index,
		},
		Operations: ops,
	})

	if nil == err {
		return resp, nil
	}

	err = fmt.Errorf(`item %v: %v`, id, err.Error())
	q.log.Error(configurable_logger.LogParameters{
		Code:    500,
		Message: "Failed to execute ElasticSearch get request",
		Data: map[string]interface{}{
			"err":     err,
			"context": q.client.GetContext(),
			"id":      id,
			"index":   index,
		},
		Operations: ops,
	})

	return nil, err
}

// Выполнение запроса поиска в ElasticSearch
func (q queryExecutor) ExecuteSearchQuery(
	index string,
	searchSource *elastic.SearchSource,
	operations []configurable_logger.LogOperation,
) (*elastic.SearchResult, error) {
	ops := q.logIncService.IncreaseOperations(operations)

	source, _ := searchSource.Source()
	jsonQuery, _ := json.Marshal(source)

	res, err := q.client.GetClient().Search(index).SearchSource(searchSource).Do(q.client.GetContext())
	if nil != err {
		err = fmt.Errorf(`query execion error: %v`, err.Error())
		q.log.Error(configurable_logger.LogParameters{
			Code:    500,
			Message: fmt.Sprintf(`Failed to execute ElasticSearch query: %v`, err.Error()),
			Data: map[string]interface{}{
				"index": index,
				"query": string(jsonQuery),
				"err":   err,
			},
			Operations: ops,
		})

		return nil, err
	}

	q.log.Debug(configurable_logger.LogParameters{
		Code:    200,
		Message: fmt.Sprintf(`Executed ElasticSearch query. Execution time: %v ms`, res.TookInMillis),
		Data: map[string]interface{}{
			"index":  index,
			"query":  string(jsonQuery),
			"result": res,
		},
		Operations: ops,
	})

	return res, nil
}

// Фабрика исполнителя запросов
func NewQueryExecutor(
	client client.ElasticClientServiceInterface,
	factory configurable_logger.TLoggerFactory,
) QueryExecutorInterface {
	return &queryExecutor{
		log:           factory(`elastic query executor`),
		logIncService: configurable_logger.LoggerOperationIncreaseService(),
		client:        client,
	}
}
