package filter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Генератор параметров фильтрации
type queryGenerator struct {
	processors []queryGeneratorProcessorInterface
}

// Генерация эквивалента переданного запроса фильтрации для ElasticSearch
func (q queryGenerator) Generate(params whereOrHavingParser.Operation) (elastic.Query, error) {
	for _, processor := range q.processors {
		if processor.isAvailable(params) {
			return processor.generate(params)
		}
	}

	return nil, fmt.Errorf(`no available processor for params type '%v'`, params.Type())
}

// Фабрика генератора
func NewQueryGenerator(object *graphql.Object, fieldsMap map[string]string) QueryGeneratorInterface {
	mapper := fieldMapper.NewFieldMapper(object, fieldsMap)
	multiProcessor := newMultiProcessor()

	generator := &queryGenerator{
		processors: []queryGeneratorProcessorInterface{
			newAndQueryGeneratorProcessor(multiProcessor),
			newOrQueryGeneratorProcessor(multiProcessor),
			newNotProcessor(multiProcessor),
			newEqualsQueryProcessor(mapper),
			newLessOrEqualsQueryProcessor(mapper),
			newLessQueryProcessor(mapper),
			newMoreOrEqualsQueryProcessor(mapper),
			newMoreQueryProcessor(mapper),
			newInQueryProcessor(mapper),
			newLikeQueryProcessor(mapper),
			newBetweenQueryProcessor(mapper),
		},
	}

	multiProcessor.setGenerator(generator)

	return generator
}
