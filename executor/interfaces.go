package executor

import (
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/olivere/elastic/v7"
)

// Сервис исполнения запросов
type QueryExecutorInterface interface {
	// Выполнение запроса поиска в ElasticSearch
	ExecuteSearchQuery(
		index string,
		searchSource *elastic.SearchSource,
		operations []configurable_logger.LogOperation,
	) (*elastic.SearchResult, error)

	// Выполнение запроса получения сущности по ID в ElasticSearch
	ExecuteGetByIdQuery(
		index string,
		id string,
		operations []configurable_logger.LogOperation,
	) (*elastic.GetResult, error)

	// Выполнение запроса удаления сущности по ID в ElasticSearch
	ExecuteRemoveByIdQuery(
		index string,
		id string,
		operations []configurable_logger.LogOperation,
	) error

	// Выполнение запроса вставки сущности в ElasticSearch
	ExecuteInsertQuery(
		index string,
		id string,
		data interface{},
		operations []configurable_logger.LogOperation,
	) (insertedId string, err error)

	// Выполнение запроса обновления сущности в ElasticSearch
	ExecuteUpdateQuery(
		index string,
		id string,
		data interface{},
		operations []configurable_logger.LogOperation,
	) error
}
