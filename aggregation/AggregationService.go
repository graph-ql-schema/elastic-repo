package aggregation

import (
	"sort"
	"sync"

	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Обработанные результаты генерации запроса
type processorGenerationResults struct {
	priority int
	result   GenerationResult
}

// Сервис обработки запросов агрегации
type aggregationService struct {
	processors []aggregationProcessorInterface
}

// Генерация запроса
func (a aggregationService) GenerateQueryByGraphQlParameters(params sbuilder.Parameters) []GenerationResult {
	var wg sync.WaitGroup
	itemsChan := make(chan processorGenerationResults)
	resultChan := make(chan []GenerationResult)
	completeChan := make(chan bool)

	defer close(itemsChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение данных генерации
	go a.readQueries(itemsChan, resultChan, completeChan)

	// Запускаем асинхронную генерацию подзапросов агрегации
	wg.Add(len(a.processors))
	for i, processor := range a.processors {
		go a.generateQuery(&wg, i, itemsChan, processor, params)
	}

	wg.Wait()
	completeChan <- true

	return <-resultChan
}

// Асинхронное чтение результатов обработки генерации запросов агрегации
func (a aggregationService) readQueries(
	itemsChan chan processorGenerationResults,
	resultChan chan []GenerationResult,
	completeChan chan bool,
) {
	data := []processorGenerationResults{}
	for {
		select {
		case item := <-itemsChan:
			data = append(data, item)
			break
		case _ = <-completeChan:
			sort.SliceStable(data, func(i, j int) bool {
				return data[i].priority < data[j].priority
			})

			result := []GenerationResult{}
			for _, item := range data {
				result = append(result, item.result)
			}

			resultChan <- result

			return
		}
	}
}

// Асинхронная обработка параметров процессором
func (a aggregationService) generateQuery(
	wg *sync.WaitGroup,
	priority int,
	itemsChan chan processorGenerationResults,
	processor aggregationProcessorInterface,
	params sbuilder.Parameters,
) {
	defer wg.Done()

	if !processor.isGenerationAvailable(params) {
		return
	}

	for i, query := range processor.generateQuery(params) {
		itemsChan <- processorGenerationResults{
			priority: priority*10000 + i,
			result:   query,
		}
	}
}

// Парсинг ответа от сервера в валидный результат
func (a aggregationService) ParseResult(
	params sbuilder.Parameters,
	data *elastic.AggregationBucketKeyItem,
) *AggregationResponse {
	var wg sync.WaitGroup
	responseChan := make(chan AggregationResponse)
	resultChan := make(chan AggregationResponse)
	completeChan := make(chan bool)

	defer close(responseChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запуск асинхронного чтения результатов
	go a.parseResults(responseChan, resultChan, completeChan)

	// Запуск асинхронного парсинга результатов
	wg.Add(len(a.processors))
	for _, processor := range a.processors {
		go a.processResults(&wg, responseChan, processor, params, data)
	}

	wg.Wait()
	completeChan <- true

	result := <-resultChan
	return &result
}

// Парсинг результатов асинхронного парсинга результата запроса
func (a aggregationService) parseResults(
	responseChan chan AggregationResponse,
	resultChan chan AggregationResponse,
	completeChan chan bool,
) {
	result := AggregationResponse{
		Count:    0,
		Avg:      nil,
		Sum:      nil,
		Min:      nil,
		Max:      nil,
		Variants: nil,
	}

	for {
		select {
		case response := <-responseChan:
			if response.Count != 0 {
				result.Count = response.Count
			}

			if response.Avg != nil {
				result.Avg = response.Avg
			}

			if response.Sum != nil {
				result.Sum = response.Sum
			}

			if response.Max != nil {
				result.Max = response.Max
			}

			if response.Min != nil {
				result.Min = response.Min
			}

			if response.Variants != nil {
				result.Variants = response.Variants
			}

			break
		case _ = <-completeChan:
			resultChan <- result

			return
		}
	}
}

// Асинхронная обработка результатов запроса
func (a aggregationService) processResults(
	wg *sync.WaitGroup,
	itemsChan chan AggregationResponse,
	processor aggregationProcessorInterface,
	params sbuilder.Parameters,
	data *elastic.AggregationBucketKeyItem,
) {
	defer wg.Done()

	response := AggregationResponse{
		Count:    0,
		Avg:      nil,
		Sum:      nil,
		Min:      nil,
		Max:      nil,
		Variants: nil,
	}

	processor.parseResult(params, &response, data)

	itemsChan <- response
}

// Фабрика сервиса
func NewAggregationService(object *graphql.Object, fieldsMap map[string]string) AggregationServiceInterface {
	return &aggregationService{
		processors: []aggregationProcessorInterface{
			newCountProcessor(),
			newMathProcessor(object, fieldsMap),
			newVariantsProcessor(object, fieldsMap),
		},
	}
}
