package aggregation

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"sync"

	"bitbucket.org/graph-ql-schema/elastic-repo/converter"
	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/requestedFieldsParser"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Тип, описывающий обработанные данные поля
type processedField struct {
	name  string
	items []interface{}
}

// Тип, описывающий обработанный вариант значений вариантного поля
type processedVariantsItem struct {
	priority int
	value    interface{}
}

// Процессор генерации запроса Variants
type variantsProcessor struct {
	fieldMapper fieldMapper.FieldMapperInterface
	converter   converter.ConverterInterface
}

// Проверка доступности процессора
func (v variantsProcessor) isGenerationAvailable(params sbuilder.Parameters) bool {
	for _, field := range params.Fields {
		if constants.VariantsOperationSchemaKey == field.GetFieldName() {
			return true
		}
	}

	return false
}

// Генерация запроса
func (v variantsProcessor) generateQuery(params sbuilder.Parameters) []GenerationResult {
	result := []GenerationResult{}
	for _, field := range params.Fields {
		if constants.VariantsOperationSchemaKey != field.GetFieldName() {
			continue
		}

		for _, subField := range field.GetSubFields() {
			mappedField := v.fieldMapper.Map(subField.GetFieldName())
			result = append(result, GenerationResult{
				Name:        v.getAggregationCode(mappedField),
				Aggregation: elastic.NewTermsAggregation().Field(mappedField),
			})
		}
	}

	return result
}

// Парсинг результата
func (v variantsProcessor) parseResult(
	params sbuilder.Parameters,
	result *AggregationResponse,
	data *elastic.AggregationBucketKeyItem,
) {
	var wg sync.WaitGroup
	itemsChan := make(chan processedField)
	completeChan := make(chan bool)

	defer close(itemsChan)
	defer close(completeChan)

	if nil == result.Variants {
		result.Variants = map[string]Variants{}
	}

	for _, field := range params.Fields {
		if constants.VariantsOperationSchemaKey != field.GetFieldName() {
			continue
		}

		// Запускаем асинхронную запись результатов для исключения проблем конкурентной записи в MAP
		go v.writeSingleField(itemsChan, completeChan, result)

		// Запускаем асинхронную обработку полей
		wg.Add(len(field.GetSubFields()))
		for _, subField := range field.GetSubFields() {
			go v.parseSingleField(&wg, itemsChan, subField, data)
		}

		wg.Wait()
		completeChan <- true

		// В коллекции полей может быть только одна группа variants
		return
	}
}

// Пишем данные в результат.
func (v variantsProcessor) writeSingleField(
	fieldsChan chan processedField,
	completeChan chan bool,
	result *AggregationResponse,
) {
	for {
		select {
		case field := <-fieldsChan:
			result.Variants[field.name] = field.items
			break
		case _ = <-completeChan:
			return
		}
	}
}

// Асинхронный парсинг данных одного поля
func (v variantsProcessor) parseSingleField(
	wg *sync.WaitGroup,
	fieldsChan chan processedField,
	field requestedFieldsParser.GraphQlRequestedFieldInterface,
	data *elastic.AggregationBucketKeyItem,
) {
	defer wg.Done()

	processedFieldData := processedField{
		name:  field.GetFieldName(),
		items: []interface{}{},
	}

	mappedField := v.fieldMapper.Map(field.GetFieldName())
	aggregation, ok := data.Aggregations[v.getAggregationCode(mappedField)]

	if !ok {
		fieldsChan <- processedFieldData
		return
	}

	var items elastic.AggregationBucketKeyItems
	err := json.Unmarshal(aggregation, &items)

	if nil != err {
		fieldsChan <- processedFieldData
		return
	}

	var wgChild sync.WaitGroup
	itemsChan := make(chan processedVariantsItem)
	resultChan := make(chan []interface{})
	completeChan := make(chan bool)

	defer close(itemsChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов
	go v.readItemVariants(itemsChan, resultChan, completeChan)

	wgChild.Add(len(items.Buckets))
	for i, subBucket := range items.Buckets {
		go v.parseItemVariant(i, &wgChild, itemsChan, field, subBucket)
	}

	wgChild.Wait()
	completeChan <- true

	processedFieldData.items = <-resultChan
	fieldsChan <- processedFieldData
}

// Асинхронное чтение результатов парсинга вариантов значений
func (v variantsProcessor) readItemVariants(
	itemsChan chan processedVariantsItem,
	resultChan chan []interface{},
	completeChan chan bool,
) {
	results := []processedVariantsItem{}
	for {
		select {
		case item := <-itemsChan:
			results = append(results, item)
			break
		case _ = <-completeChan:
			result := []interface{}{}
			sort.SliceStable(results, func(i, j int) bool {
				return results[i].priority < results[j].priority
			})

			for _, processed := range results {
				result = append(result, processed.value)
			}

			resultChan <- result

			return
		}
	}
}

// Парсинг одного бакета варианта значений. Работает асинхронно
func (v variantsProcessor) parseItemVariant(
	num int,
	wg *sync.WaitGroup,
	itemsChan chan processedVariantsItem,
	field requestedFieldsParser.GraphQlRequestedFieldInterface,
	item *elastic.AggregationBucketKeyItem,
) {
	defer wg.Done()

	var val interface{} = item.Key
	if nil != item.KeyAsString {
		val = *item.KeyAsString
	}

	convertedItem, err := v.converter.Convert(field.GetFieldName(), val)
	if nil != err {
		return
	}

	if reflect.TypeOf(convertedItem).Kind() != reflect.Slice {
		itemsChan <- processedVariantsItem{
			priority: num * 10000,
			value:    convertedItem,
		}
		return
	}

	s := reflect.ValueOf(convertedItem)
	for i := 0; i < s.Len(); i++ {
		itemsChan <- processedVariantsItem{
			priority: num*10000 + i,
			value:    s.Index(i).Interface(),
		}
	}
}

// Генерация уникального кода агрегации по переданным параметрам названия поля и операции
func (v variantsProcessor) getAggregationCode(fieldName string) string {
	return fmt.Sprintf(`%v_aggregation_operation_variants`, fieldName)
}

// Фабрика процессора
func newVariantsProcessor(object *graphql.Object, fieldsMap map[string]string) aggregationProcessorInterface {
	return &variantsProcessor{
		fieldMapper: fieldMapper.NewFieldMapper(object, fieldsMap),
		converter:   converter.NewConverter(object),
	}
}
