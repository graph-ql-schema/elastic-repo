package converter

import (
	"reflect"
	"testing"
)

func TestConverter_Convert(t *testing.T) {
	type fields struct {
		processors []converterProcessorInterface
	}
	type args struct {
		field string
		val   interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование на не доступном процессоре",
			fields: fields{
				processors: []converterProcessorInterface{
					&converterProcessorMock{
						IsAvailable: false,
						IsError:     false,
						Result:      "test",
					},
				},
			},
			args: args{
				field: "test",
				val:   "test",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на доступном процессоре, возвращающем ошибку",
			fields: fields{
				processors: []converterProcessorInterface{
					&converterProcessorMock{
						IsAvailable: true,
						IsError:     true,
						Result:      "test",
					},
				},
			},
			args: args{
				field: "test",
				val:   "test",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на доступном процессоре",
			fields: fields{
				processors: []converterProcessorInterface{
					&converterProcessorMock{
						IsAvailable: true,
						IsError:     false,
						Result:      "test",
					},
				},
			},
			args: args{
				field: "test",
				val:   "test",
			},
			want:    "test",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := converter{
				processors: tt.fields.processors,
			}
			got, err := c.Convert(tt.args.field, tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("Convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
