package queries

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/graph-ql-schema/elastic-repo/filter"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Генератор запросов на получение ID сущностей по переданным параметрам фильтрации
type entityIdsQueryBuilder struct {
	logger        configurable_logger.LoggerInterface
	logIncService configurable_logger.LoggerOperationIncreaseServiceInterface
	queryBuilder  filter.QueryGeneratorInterface
}

// Построение запроса листинга сущностей
func (e entityIdsQueryBuilder) Build(
	filter whereOrHavingParser.Operation,
	operations []configurable_logger.LogOperation,
) (*elastic.SearchSource, error) {
	startsGeneration := time.Now().UnixNano() / int64(time.Millisecond)
	ops := e.logIncService.IncreaseOperations(operations)
	result, err := e.parseFilterParameters(elastic.NewSearchSource(), filter, ops)
	if nil != err {
		return nil, err
	}

	source, _ := result.Source()
	jsonSource, _ := json.Marshal(source)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	e.logger.Debug(configurable_logger.LogParameters{
		Code:    200,
		Message: fmt.Sprintf(`Generated entity ids list query. Execution time: %v ms`, finish-startsGeneration),
		Data: map[string]interface{}{
			"query":  string(jsonSource),
			"filter": filter,
		},
		Operations: ops,
	})

	return result, nil
}

// Парсинг параметров Where запроса
func (e entityIdsQueryBuilder) parseFilterParameters(
	source *elastic.SearchSource,
	filter whereOrHavingParser.Operation,
	operations []configurable_logger.LogOperation,
) (*elastic.SearchSource, error) {
	if nil == filter {
		return source, nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query, err := e.queryBuilder.Generate(filter)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		err = fmt.Errorf(`filter parsing error: %v`, err.Error())
		e.logger.Warning(configurable_logger.LogParameters{
			Code:    400,
			Message: fmt.Sprintf(`Failed to parse filter request: %v`, err.Error()),
			Data: map[string]interface{}{
				"filter": filter,
				"err":    err,
			},
			Operations: operations,
		})

		return nil, err
	}

	e.logger.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Built filter query. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"filter": filter,
		},
		Operations: operations,
	})

	return source.Query(query), nil
}

// Парсинг ответа на запрос
func (e entityIdsQueryBuilder) Parse(
	response *elastic.SearchResult,
	operations []configurable_logger.LogOperation,
) []string {
	ops := e.logIncService.IncreaseOperations(operations)

	starts := time.Now().UnixNano() / int64(time.Millisecond)

	ids := []string{}
	for _, hit := range response.Hits.Hits {
		ids = append(ids, hit.Id)
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)

	e.logger.Debug(configurable_logger.LogParameters{
		Code:    100,
		Message: fmt.Sprintf("Parsed ids. Execution time: %v ms", finish-starts),
		Data: map[string]interface{}{
			"ids": ids,
		},
		Operations: ops,
	})

	return ids
}

// Фабрика генератора запросов
func NewEntityIdsQueryBuilder(
	object *graphql.Object,
	fieldsMap map[string]string,
	factory configurable_logger.TLoggerFactory,
) EntityIdsQueryBuilderInterface {
	return &entityIdsQueryBuilder{
		logger:        factory(`entityIdsQueryBuilder`),
		logIncService: configurable_logger.LoggerOperationIncreaseService(),
		queryBuilder:  filter.NewQueryGenerator(object, fieldsMap),
	}
}
