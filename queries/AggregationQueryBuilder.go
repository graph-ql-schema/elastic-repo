package queries

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/graph-ql-schema/elastic-repo/aggregation"
	"bitbucket.org/graph-ql-schema/elastic-repo/filter"
	"bitbucket.org/graph-ql-schema/elastic-repo/groupBy"
	"bitbucket.org/graph-ql-schema/sbuilder"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/sveshnikovwork/configurable-logger"
	"github.com/graphql-go/graphql"
	"github.com/olivere/elastic/v7"
)

// Генератор запроса агрегации
type aggregationQueryBuilder struct {
	filterBuilder  filter.QueryGeneratorInterface
	groupByBuilder groupBy.GroupByServiceInterface
	log            configurable_logger.LoggerInterface
	logIncService  configurable_logger.LoggerOperationIncreaseServiceInterface
}

// Парсинг ответа на запрос
func (a aggregationQueryBuilder) Parse(
	params sbuilder.Parameters,
	response *elastic.SearchResult,
	operations []configurable_logger.LogOperation,
) ([]*aggregation.AggregationResponse, error) {
	ops := a.logIncService.IncreaseOperations(operations)

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	result := a.groupByBuilder.ParseResponse(params, response)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	a.log.Debug(configurable_logger.LogParameters{
		Code:    200,
		Message: fmt.Sprintf(`Parsed aggregation response. Execution time: %v ms`, finish-starts),
		Data: map[string]interface{}{
			"result": result,
			"params": map[string]interface{}{
				"arguments": params.Arguments,
				"fields":    params.Fields,
			},
		},
		Operations: ops,
	})

	if nil != params.Arguments.Pagination {
		if 0 != params.Arguments.Pagination.Limit {
			result = result[params.Arguments.Pagination.Offset:]
			if len(result) > int(params.Arguments.Pagination.Limit) {
				result = result[:params.Arguments.Pagination.Limit]
			}
		}
	}

	return result, nil
}

// Построение запроса агрегации
func (a aggregationQueryBuilder) Build(
	params sbuilder.Parameters,
	operations []configurable_logger.LogOperation,
) (*elastic.SearchSource, error) {
	ops := a.logIncService.IncreaseOperations(operations)

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query := elastic.NewSearchSource()
	filterQuery, err := a.getFilterQuery(params)
	if nil != err {
		err = fmt.Errorf(`aggregationQueryBuilder failed to build query: %v`, err.Error())
		a.log.Warning(configurable_logger.LogParameters{
			Code:    400,
			Message: fmt.Sprintf(`Failed to generate query: %v`, err.Error()),
			Data: map[string]interface{}{
				"error": err,
				"params": map[string]interface{}{
					"arguments": params.Arguments,
					"fields":    params.Fields,
				},
			},
			Operations: ops,
		})
		return nil, err
	}

	query = query.
		Query(filterQuery).
		Aggregation(a.groupByBuilder.GenerateQuery(params)).
		Size(0).Sort("active", true)

	finish := time.Now().UnixNano() / int64(time.Millisecond)

	source, _ := query.Source()
	jsonQuery, _ := json.Marshal(source)

	a.log.Debug(configurable_logger.LogParameters{
		Code:    200,
		Message: fmt.Sprintf(`Generated aggregation query. Execution time: %v ms`, finish-starts),
		Data: map[string]interface{}{
			"query": string(jsonQuery),
			"params": map[string]interface{}{
				"arguments": params.Arguments,
				"fields":    params.Fields,
			},
		},
		Operations: ops,
	})

	return query, nil
}

// Генерация параметров фильтрации для запроса
func (a aggregationQueryBuilder) getFilterQuery(params sbuilder.Parameters) (elastic.Query, error) {
	if nil == params.Arguments.Having && nil == params.Arguments.Where {
		return elastic.NewMatchAllQuery(), nil
	}

	var filterOperation whereOrHavingParser.Operation

	switch true {
	case nil != params.Arguments.Having && nil != params.Arguments.Where:
		operations := []whereOrHavingParser.Operation{}

		havingOps := params.Arguments.Having.Value().([]whereOrHavingParser.Operation)
		for _, operation := range havingOps {
			operations = append(operations, operation)
		}

		whereOps := params.Arguments.Where.Value().([]whereOrHavingParser.Operation)
		for _, operation := range whereOps {
			operations = append(operations, operation)
		}

		filterOperation = whereOrHavingParser.NewAndOrOperation(
			constants.AndSchemaKey,
			operations,
			constants.WhereSchemaKey,
		)
		break

	case nil != params.Arguments.Having:
		filterOperation = params.Arguments.Having
		break

	case nil != params.Arguments.Where:
		filterOperation = params.Arguments.Where
		break
	}

	if nil == filterOperation {
		return nil, fmt.Errorf(`no filter operation collected from parameters`)
	}

	return a.filterBuilder.Generate(filterOperation)
}

// Фабрика сервиса
func NewAggregationQueryBuilder(
	object *graphql.Object,
	fieldsMap map[string]string,
	factory configurable_logger.TLoggerFactory,
) AggregationQueryBuilderInterfaces {
	return &aggregationQueryBuilder{
		filterBuilder:  filter.NewQueryGenerator(object, fieldsMap),
		groupByBuilder: groupBy.NewGroupByService(object, fieldsMap),
		log:            factory(fmt.Sprintf(`aggregationQueryBuilder (%v)`, object.Name())),
		logIncService:  configurable_logger.LoggerOperationIncreaseService(),
	}
}
