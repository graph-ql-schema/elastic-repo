package filter

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/elastic-repo/fieldMapper"
	"bitbucket.org/graph-ql-schema/sbuilder/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/olivere/elastic/v7"
)

// Тестирование доступности процессора
func Test_searchQueryProcessor_isAvailable(t *testing.T) {
	type fields struct {
		fieldsMapper fieldMapper.FieldMapperInterface
		factory      tSearchFactory
		operator     string
	}
	type args struct {
		params whereOrHavingParser.Operation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не доступной операции",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
				factory: func(s string, i interface{}) elastic.Query {
					return elastic.NewTermQuery(s, i)
				},
				operator: constants.EqualsSchemaKey,
			},
			args: args{
				params: operationMock{
					rType: "test",
				},
			},
			want: false,
		},
		{
			name: "Тестирование на доступной операции",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
				factory: func(s string, i interface{}) elastic.Query {
					return elastic.NewTermQuery(s, i)
				},
				operator: constants.EqualsSchemaKey,
			},
			args: args{
				params: operationMock{
					rType: constants.EqualsSchemaKey,
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := searchQueryProcessor{
				fieldsMapper: tt.fields.fieldsMapper,
				factory:      tt.fields.factory,
				operator:     tt.fields.operator,
			}
			if got := e.isAvailable(tt.args.params); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_searchQueryProcessor_generate(t *testing.T) {
	type fields struct {
		fieldsMapper fieldMapper.FieldMapperInterface
		factory      tSearchFactory
		operator     string
	}
	type args struct {
		params whereOrHavingParser.Operation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    elastic.Query
		wantErr bool
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				fieldsMapper: fieldMapper.FieldMapperMock{},
				factory: func(s string, i interface{}) elastic.Query {
					return elastic.NewTermQuery(s, i)
				},
				operator: constants.EqualsSchemaKey,
			},
			args: args{
				params: operationMock{
					rType:  constants.EqualsSchemaKey,
					rValue: "test",
				},
			},
			want:    elastic.NewTermQuery("test_field", "test"),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := searchQueryProcessor{
				fieldsMapper: tt.fields.fieldsMapper,
				factory:      tt.fields.factory,
				operator:     tt.fields.operator,
			}
			got, err := e.generate(tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
