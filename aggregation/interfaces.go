package aggregation

import (
	"bitbucket.org/graph-ql-schema/sbuilder"
	"github.com/olivere/elastic/v7"
)

// Результат работы процессора
type GenerationResult struct {
	Name        string
	Aggregation elastic.Aggregation
}

// Тип, описывающий варианты значений сущности
type Variants = []interface{}

// Тип, описывающий
type AggregationResponse struct {
	Count    int64
	Avg      map[string]float64
	Sum      map[string]float64
	Min      map[string]float64
	Max      map[string]float64
	Variants map[string]Variants
}

// Сервис обработки запросов агрегации
type AggregationServiceInterface interface {
	// Генерация запроса
	GenerateQueryByGraphQlParameters(params sbuilder.Parameters) []GenerationResult

	// Парсинг ответа от сервера в валидный результат
	ParseResult(params sbuilder.Parameters, data *elastic.AggregationBucketKeyItem) *AggregationResponse
}

// Процессор генерации запросов агрегации
type aggregationProcessorInterface interface {
	// Проверка доступности процессора
	isGenerationAvailable(params sbuilder.Parameters) bool

	// Генерация запроса
	generateQuery(params sbuilder.Parameters) []GenerationResult

	// Парсинг результата
	parseResult(params sbuilder.Parameters, result *AggregationResponse, data *elastic.AggregationBucketKeyItem)
}
